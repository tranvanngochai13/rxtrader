import Image from "next/image";
import React from "react";
import { Container } from "../common/Container";
import { LineTitle } from "../common/vme-expo-show/LineTitle";

export default function ShowLocation({
  showContent = false,
  map,
  widthImage,
  heightImage,
  weightTitle = "normal",
}) {
  return (
    <div className="bg-white py-10">
      <Container>
        <div className="flex flex-col gap-6">
          <LineTitle
            title="Show Location"
            colorLine="DarkRed"
            Weight={weightTitle}
            textColor="#000000"
          />
          {showContent && (
            <div className="text-[1.063rem] leading-[171%] text-center px-[1.813rem]">
              <p>I.C.E. Hanoi (Cung Van Hoa)</p>
              <p>91 Tran Hung Dao Street, Hanoi, Vietnam</p>
              <p>
                Located in the heart of Hanoi, the center of easy-to-access
                transportation links, I.C.E. Hanoi is built with heavy machinery
                exhibitions in mind. Its facilities and services will make your
                exhibiting or visiting experience very productive and
                convenient.
              </p>
            </div>
          )}
          <Image
            src={map}
            alt="image"
            width={widthImage}
            height={heightImage}
            style={{ width: "100%", height: "auto", objectFit: "cover" }}
          />
        </div>
      </Container>
    </div>
  );
}
