import Image from "next/image";
import React from "react";
import { Container } from "../common/Container";
import { LineTitle } from "../common/vme-expo-show/LineTitle";

export default function AboutRXTradex({
  content,
  headingColor = "#333333",
  contentColor = "#4E4D4D",
  backGround = "transparent",
  fontSize = "1.188rem",
  lineHeight = "153%",
}) {
  return (
    <div style={{ backgroundColor: `${backGround}` }} className="py-10">
      <Container>
        <div className="flex flex-col gap-[1.875rem] text-center">
          <div className="flex flex-col gap-[1.25rem]">
            <LineTitle
              title="About RX Tradex"
              colorLine="DarkRed"
              textColor={headingColor}
            />
            <p
              style={{
                color: `${contentColor}`,
                fontSize: `${fontSize}`,
                lineHeight: `${lineHeight}`,
              }}
            >
              {content}
            </p>
          </div>
          <div>
            <Image
              src="/images/VMEExpoShow/BG About RX Tradex.png"
              alt="image"
              width={1136}
              height={533.83}
              style={{ width: "100%", height: "auto", objectFit: "cover" }}
            />
          </div>
        </div>
      </Container>
    </div>
  );
}
