import React from "react";

export default function FormRegistration() {
  return (
    <div className="w-full max-w-[37rem] py-[1.969rem] px-[3.656rem] bg-white shadow-[0px_3px_20px_rgba(0,0,0,0.13)]">
      <form className="flex flex-col gap-[1.875rem]">
        <div>
          <label
            htmlFor="input-name"
            className="block text-lg font-bold leading-none mb-[0.313rem]"
          >
            Name
          </label>
          <input
            id="input-name"
            name="name"
            type="text"
            className="block w-full px-[0.625rem] py-[0.938rem] text-[0.938rem] font-medium italic leading-none text-[rgba(0,0,0,0.2)] bg-white border border-[#C9C9C9] rounded-[0.313rem] outline-none focus:outline-none"
            placeholder="Your Email Here"
          />
        </div>
        <div>
          <label
            htmlFor="input-email"
            className="block text-lg font-bold leading-none mb-[0.313rem]"
          >
            Email Address
          </label>
          <input
            id="input-email"
            name="email"
            type="mail"
            className="block w-full px-[0.625rem] py-[0.938rem] text-[0.938rem] font-medium italic leading-none text-[rgba(0,0,0,0.2)] bg-white border border-[#C9C9C9] rounded-[0.313rem] outline-none focus:outline-none"
            placeholder="Your Email Here"
          />
        </div>
        <div>
          <label
            htmlFor="input-phone"
            className="block text-lg font-bold leading-none mb-[0.313rem]"
          >
            Telephone
          </label>
          <input
            id="input-phone"
            name="phone"
            type="text"
            className="block w-full px-[0.625rem] py-[0.938rem] text-[0.938rem] font-medium italic leading-none text-[rgba(0,0,0,0.2)] bg-white border border-[#C9C9C9] rounded-[0.313rem] outline-none focus:outline-none"
            placeholder="Your Email Here"
          />
        </div>
        <div>
          <label
            htmlFor="input-company"
            className="block text-lg font-bold leading-none mb-[0.313rem]"
          >
            Company Name
          </label>
          <input
            id="input-company"
            name="company"
            type="text"
            className="block w-full px-[0.625rem] py-[0.938rem] text-[0.938rem] font-medium italic leading-none text-[rgba(0,0,0,0.2)] bg-white border border-[#C9C9C9] rounded-[0.313rem] outline-none focus:outline-none"
            placeholder="Your Email Here"
          />
        </div>
        <div>
          <label
            htmlFor="input-job"
            className="block text-lg font-bold leading-none mb-[0.313rem]"
          >
            Job tittle
          </label>
          <input
            id="input-job"
            name="job"
            type="text"
            className="block w-full px-[0.625rem] py-[0.938rem] text-[0.938rem] font-medium italic leading-none text-[rgba(0,0,0,0.2)] bg-white border border-[#C9C9C9] rounded-[0.313rem] outline-none focus:outline-none"
            placeholder="Your Email Here"
          />
        </div>

        <div>
          <label className="block text-lg font-bold leading-none mb-[0.313rem]">
            Which type of Participation do you choose?
          </label>
          <div>
            <div class="flex items-center py-[0.844rem] px-[0.625rem] mb-[0.313rem] bg-white border border-[#C9C9C9] rounded-[0.313rem]">
              <input
                defaultChecked
                id="default-radio-1"
                type="radio"
                defaultValue
                name="default-radio"
                className="w-4 h-4 accent-DarkRed outline-none focus:outline-none"
              />
              <label
                htmlFor="default-radio-1"
                className="ml-[0.625rem] text-[0.938rem] leading-[120%]"
              >
                As an exhibitor
              </label>
            </div>
            <div class="flex items-center py-[0.844rem] px-[0.625rem] mb-[0.313rem] bg-white border border-[#C9C9C9] rounded-[0.313rem]">
              <input
                id="default-radio-2"
                type="radio"
                defaultValue
                name="default-radio"
                className="w-4 h-4 accent-DarkRed outline-none focus:outline-none"
              />
              <label
                htmlFor="default-radio-2"
                className="ml-[0.625rem] text-[0.938rem] leading-[120%]"
              >
                As a visitor
              </label>
            </div>
          </div>
        </div>

        <div>
          <label className="block text-lg font-bold leading-none mb-[0.313rem]">
            Privacy
          </label>
          <div className="flex items-center py-[0.844rem] px-[0.625rem] mb-[0.313rem] bg-white border border-[#C9C9C9] rounded-[0.313rem]">
            <input
              id="link-checkbox"
              type="checkbox"
              defaultValue
              className="w-4 h-4 accent-DarkRed border-[1.5px] border-solid border-[#2D2D2D]"
            />
            <label
              htmlFor="link-checkbox"
              className="ml-[0.625rem] text-[0.938rem] leading-[120%]"
            >
              I confirm that I have read and agree to the {""}
              <a href="#" className="text-DarkRed hover:underline">
                privacy policy
              </a>
              .
            </label>
          </div>
          <span className="text-[0.813rem] text-DarkRed leading-none italic">
            You have to read and agree to the Privacy policy
          </span>
        </div>
      </form>
    </div>
  );
}
