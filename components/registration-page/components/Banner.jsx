import Image from "next/image";
import React from "react";
import { Container } from "../../common/Container";

export default function Banner() {
  return (
    <div
      style={{
        backgroundImage: `url("images/Registration/bg-banner.png")`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
      }}
      className="py-[2.969rem]"
    >
      <Container>
        <div className="flex items-center gap-10">
          <Image
            src="/images/Registration/logo.png"
            alt="image"
            width={266}
            height={169}
            style={{ objectFit: "cover" }}
          />
          <div className="flex flex-col gap-[1.875rem]">
            <div className="text-[2.5rem] leading-[130%] uppercase">
              <h2 className="text-white mb-[0.313rem]">
                EMBRACING HYPERCONNECTIVITY OF
              </h2>
              <h2 className="text-DarkRed">MANUFACTURING INNOVATION</h2>
            </div>
            <div className="flex flex-col text-[1.313rem] text-white leading-[130%]">
              <div className="flex items-center">
                <div className="w-[1.625rem] pr-[0.375rem] flex justify-center">
                  <svg
                    width="20"
                    height="28"
                    viewBox="0 0 20 28"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M17.3188 5.44922H15.35V3.31641C15.35 3.07031 15.1039 2.82422 14.8578 2.82422H13.2172C12.9301 2.82422 12.725 3.07031 12.725 3.31641V5.44922H7.475V3.31641C7.475 3.07031 7.22891 2.82422 6.98281 2.82422H5.34219C5.05508 2.82422 4.85 3.07031 4.85 3.31641V5.44922H2.88125C1.77383 5.44922 0.9125 6.35156 0.9125 7.41797V21.8555C0.9125 22.9629 1.77383 23.8242 2.88125 23.8242H17.3188C18.3852 23.8242 19.2875 22.9629 19.2875 21.8555V7.41797C19.2875 6.35156 18.3852 5.44922 17.3188 5.44922ZM17.0727 21.8555H3.12734C2.96328 21.8555 2.88125 21.7734 2.88125 21.6094V9.38672H17.3188V21.6094C17.3188 21.7734 17.1957 21.8555 17.0727 21.8555Z"
                      fill="#CD1719"
                    />
                  </svg>
                </div>
                <span>WED-FRI 09-11 AUG 2023 9.00 - 17.00 Hrs</span>
              </div>
              <div className="flex items-center">
                <div className="w-[1.625rem] pr-[0.375rem] flex justify-center">
                  <svg
                    width="16"
                    height="22"
                    viewBox="0 0 16 22"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M7.30469 21.0801C7.67383 21.6543 8.53516 21.6543 8.9043 21.0801C14.8926 12.4668 16 11.5645 16 8.36523C16 4.01758 12.4727 0.490234 8.125 0.490234C3.73633 0.490234 0.25 4.01758 0.25 8.36523C0.25 11.5645 1.31641 12.4668 7.30469 21.0801ZM8.125 11.6465C6.2793 11.6465 4.84375 10.2109 4.84375 8.36523C4.84375 6.56055 6.2793 5.08398 8.125 5.08398C9.92969 5.08398 11.4062 6.56055 11.4062 8.36523C11.4062 10.2109 9.92969 11.6465 8.125 11.6465Z"
                      fill="#CD1719"
                    />
                  </svg>
                </div>

                <span>
                  I.C.E Hanoi (Cung Van Hoa), 91 Tran Hung Dao street, Hanoi
                  Vietnam
                </span>
              </div>
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
}
