import React from "react";
import { Container } from "../common/Container";
import FormRegistration from "./components/FormRegistration";

export default function Registration() {
  return (
    <div className="py-10">
      <Container>
        <div className="flex flex-col items-center gap-10">
          <div className="text-center">
            <div className="mb-[1.875rem]">
              <h2 className="text-[3.75rem] font-bold leading-[116.67%] mb-[0.625rem]">
                Register your interest as an Exhibitor
              </h2>
              <h3 className="text-[2.5rem] font-bold leading-none">
                Vietnam Manufacturing Expo 2023
              </h3>
            </div>
            <p className="text-[1.063rem] leading-[120%] mb-[0.625rem]">
              This is an expression of interest form. By filling in and
              submitting this form you are simply sharing your interest to
              explore the chance of exhibiting at VME - Vietnam Manufacturing
              Expo 2023.
            </p>
            <p className="text-[1.063rem] font-bold leading-[120%]">
              By registering your interest in VME - Vietnam Manufacturing Expo
              you will receive information about the show.‎
            </p>
          </div>
          <FormRegistration />
        </div>
      </Container>
    </div>
  );
}
