import Image from "next/image";
import React from "react";
import { Container } from "../../common/Container";
import { LineTitle } from "../../common/vme-expo-show/LineTitle";

const listMeadia = [
  {
    id: 1,
    image: "/images/NEPCON Event/media-1.png",
  },
  {
    id: 2,
    image: "/images/NEPCON Event/media-2.png",
  },
  {
    id: 3,
    image: "/images/NEPCON Event/media-3.png",
  },
  {
    id: 4,
    image: "/images/NEPCON Event/media-4.png",
  },
  {
    id: 5,
    image: "/images/NEPCON Event/media-5.png",
  },
  {
    id: 6,
    image: "/images/NEPCON Event/media-6.png",
  },
  {
    id: 7,
    image: "/images/NEPCON Event/media-7.png",
  },
  {
    id: 8,
    image: "/images/NEPCON Event/media-8.png",
  },
  {
    id: 9,
    image: "/images/NEPCON Event/media-9.png",
  },
  {
    id: 10,
    image: "/images/NEPCON Event/media-10.png",
  },
  {
    id: 11,
    image: "/images/NEPCON Event/media-11.png",
  },
  {
    id: 12,
    image: "/images/NEPCON Event/media-12.png",
  },
  {
    id: 13,
    image: "/images/NEPCON Event/media-13.png",
  },
  {
    id: 14,
    image: "/images/NEPCON Event/media-14.png",
  },
  {
    id: 15,
    image: "/images/NEPCON Event/media-15.png",
  },
  {
    id: 16,
    image: "/images/NEPCON Event/media-16.png",
  },
  {
    id: 17,
    image: "/images/NEPCON Event/media-17.png",
  },
  {
    id: 18,
    image: "/images/NEPCON Event/media-18.png",
  },
];

export default function MediaPartner() {
  return (
    <div className="py-10 bg-white">
      <Container>
        <div className="flex flex-col gap-[1.875rem]">
          <LineTitle
            title="Media Partner"
            colorLine="DarkRed"
            textColor="black"
            Weight="bold"
          />
          <div className="flex justify-center flex-wrap gap-4">
            {listMeadia.map((item) => {
              return (
                <div key={item.id} className="h-[5.625rem] w-auto shrink-0">
                  <Image
                    src={item.image}
                    alt="image"
                    width={272}
                    height={90}
                    style={{
                      width: "100%",
                      height: "auto",
                      objectFit: "cover",
                    }}
                  />
                </div>
              );
            })}
          </div>
        </div>
      </Container>
    </div>
  );
}
