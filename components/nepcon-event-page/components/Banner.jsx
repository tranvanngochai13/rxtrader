import Image from "next/image";
import React from "react";
import { Button } from "../../common/Button";
import { Container } from "../../common/Container";
import {
  ExhibitorRegistrationIcon,
  VisitorRegistrationIcon,
} from "../../../assets/svg";

export default function Banner() {
  return (
    <div
      style={{
        backgroundImage: `url("images/NEPCON Event/NEPCON Tittle Screen.png")`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
      }}
      className="py-[5.064rem]"
    >
      <Container>
        <div className="mx-auto max-w-[35rem] flex flex-col items-center gap-[0.938rem]">
          <Image
            src="/images/NEPCON Event/logo.png"
            alt="image"
            width={560}
            height={115}
            style={{ objectFit: "cover" }}
          />

          <Image
            src="/images/NEPCON Event/Hanoi Inf.png"
            alt="image"
            width={560}
            height={124.95}
            style={{ objectFit: "cover" }}
          />

          <Image
            src="/images/NEPCON Event/HCM Inf.png"
            alt="image"
            width={560}
            height={132}
            style={{ objectFit: "cover" }}
          />

          <div className="flex flex-col items-center gap-[0.938rem] w-full">
            <Button
              name="Exhibitor Registration"
              icon={<ExhibitorRegistrationIcon />}
              background="#CD1719"
              colorBorder="#9C1415"
              rounded
              uppercase
            />
            <Button
              name="Exhibitor Registration"
              icon={<VisitorRegistrationIcon />}
              background="#EA921F"
              colorBorder="#A56D17"
              rounded
              uppercase
            />
          </div>
        </div>
      </Container>
    </div>
  );
}
