import Image from "next/image";
import React from "react";
import { Container } from "../../common/Container";
import { LineTitle } from "../../common/vme-expo-show/LineTitle";

export default function MedicalSupporter() {
  return (
    <div className="py-10 bg-[#EFEFEF]">
      <Container>
        <div className="flex flex-col items-center gap-[1.875rem]">
          <LineTitle
            title="Medical Supporter"
            colorLine="#CD1719"
            Weight="bold"
          />
          <Image
            src="/images/NEPCON Event/Logo-Medical Supporter.png"
            alt="image"
            width={368}
            height={90}
            style={{ objectFit: "cover" }}
          />
        </div>
      </Container>
    </div>
  );
}
