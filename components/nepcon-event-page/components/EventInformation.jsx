import Image from "next/image";
import React from "react";
import { Container } from "../../common/Container";
import { Button } from "../../common/Button";

const listEventInformation = [
  {
    id: 1,
    image: "/images/NEPCON Event/Info-1.png",
  },
  {
    id: 2,
    image: "/images/NEPCON Event/Info-2.png",
  },
  {
    id: 3,
    image: "/images/NEPCON Event/Info-3.png",
  },
  {
    id: 4,
    image: "/images/NEPCON Event/Info-4.png",
  },
];

export default function EventInformation() {
  return (
    <div
      style={{
        backgroundImage: `url("images/NEPCON Event/Event Information.png")`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
      }}
      className="py-10"
    >
      <Container>
        <div className="flex flex-col items-center gap-[1.875rem] text-center">
          <p className="text-[1.188rem] text-white leading-[153%] px-[0.547rem]">
            NEPCON VIETNAM is the most influential and only Electronic Trade
            Show in Vietnam — the proving ground for latest technologies
            introduction and global innovators. This is where more than 300
            brands do business and meet new partners, and the sharpest
            innovators hit the stage.​
          </p>
          <p className="text-[1.188rem] text-white leading-[153%] px-[1.438rem]">
            NEPCON VIETNAM 2023 focuses on the innovative concept of “Emerging
            Trends In Electronics - The Future Of Global Supply Chains". The
            exhibition will bring together 300 brands and companies to showcase
            - SMT, Testing Technologies, Equipment, Supporting Industries,
            Intelligent Manufacturing and other relevant areas will also be
            exhibited.​
          </p>
          <div className="grid grid-cols-2 items-center justify-center gap-4 max-w-[47rem]">
            {listEventInformation.map((item) => {
              return (
                <div key={item.id} className="w-full h-auto shrink-0">
                  <Image
                    src={item.image}
                    alt="image"
                    width={368}
                    height={151}
                    style={{
                      width: "100%",
                      height: "auto",
                      objectFit: "cover",
                    }}
                  />
                </div>
              );
            })}
          </div>

          <div className="flex items-center gap-[1.875rem]">
            <Button
              name="Reserve space"
              background="#EC6523"
              colorBorder="#B8501E"
              rounded
            />
            <Button
              name="Pre-Register to visit"
              background="#EC6523"
              colorBorder="#B8501E"
              rounded
            />
          </div>
        </div>
      </Container>
    </div>
  );
}
