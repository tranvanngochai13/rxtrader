import Image from "next/image";
import React from "react";
import { Container } from "../../common/Container";

export default function Opportunities() {
  return (
    <div
      style={{
        backgroundImage: `url("images/NEPCON Event/Opportunities.png")`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
      }}
      className="py-10"
    >
      <Container>
        <div className="flex flex-col items-center gap-[1.875rem] text-white text-center">
          <div className="relative min-h-[9.25rem] max-w-[54.75rem] w-full">
            <h2 className="text-[1.563rem] font-bold leading-[136%] max-w-[46.125rem] text-left uppercase">
              Golden Opportunities to join with us visiting potential buyers via
              factory excursion program 2023
            </h2>
            <div className="absolute w-auto h-auto shrink-0 right-0 top-0">
              <Image
                src="/images/NEPCON Event/line-title.png"
                alt="image"
                width={545}
                height={148}
                style={{ objectFit: "cover" }}
              />
            </div>
          </div>
          <p className="text-[1.188rem] leading-[153%]">
            Factory Visit Trip 2023 is created in order to give our both Visitor
            and Exhibitor a satisfying experience during the factory visit where
            they will be toured in various inspection and testing facilities
            which will allow them to observe and understand on how the product
            is manufactured and how to operate the manufacturer. The important
            criteria for selecting targeted factories are high digitization rate
            of more than 40%, applying new and high-tech machines and
            technologies, and currently is the model of factories for
            industrialization - modernization, they are also having full
            competences and ability to participate in the global supply chain
            for FDI.
          </p>
          <h2 className="text-[2.5rem] font-bold leading-[130%]">
            This Trip will bring you the great Opportunities:
          </h2>
          <div className="flex flex-col items-center gap-[0.625rem]">
            <h3 className="text-[0.938rem] font-semibold italic leading-none">
              Disclaimer:
            </h3>
            <div className="text-base leading-[150%]">
              <p>
                This trip is designed with very limited invitation, only those
                who complete the registration process, receive notifications and
                direct communicated with confirmation from Organizer will
                obligated to participate.
              </p>
              <p>
                The content/program is subject to changes without prior notice.
              </p>
            </div>
          </div>
          <Image
            src="/images/NEPCON Event/BIZ2_Website-22.png"
            alt="image"
            width={1136}
            height={479.59}
            style={{ width: "100%", height: "auto", objectFit: "cover" }}
          />
          <h2 className="text-[2.5rem] font-bold leading-[130%]">
            Register now to join in our Factory Visit 2023 in Thailand around
            June and in Vietnam in July!
          </h2>
          <div className="max-w-[13.25rem] text-center flex items-center flex-col gap-[0.625rem]">
            <div className="w-auto h-auto shrink-0">
              <Image
                src="/images/NEPCON Event/qr.png"
                alt="image"
                width={186}
                height={241}
                style={{ objectFit: "cover" }}
              />
            </div>

            <h3>Contact: </h3>
            <ul className="text-base leading-[150%]">
              <li className="font-bold">Mr. Trần Quốc Phong (Bruce)</li>
              <li>Business Delegation</li>
              <li>T: +84 28 6287 3355 Ext: 116</li>
              <li>M: +84 907 775 275</li>
              <li>E: phong.tran@rxtradex.com</li>
            </ul>
          </div>
        </div>
      </Container>
    </div>
  );
}
