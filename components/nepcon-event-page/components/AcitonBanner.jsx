import Image from "next/image";
import React from "react";
import { Button } from "../../common/Button";
import { Container } from "../../common/Container";

export default function AcitonBanner() {
  return (
    <div
      style={{
        backgroundImage: `url("images/NEPCON Event/Aciton Banner.png")`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
      }}
      className="py-[1.25rem]"
    >
      <Container>
        <div className="flex flex-col items-center gap-[0.625rem]">
          <Image
            src="/images/NEPCON Event/topic-main.png"
            alt="image"
            width={653}
            height={103}
            style={{ objectFit: "cover" }}
          />
          <Button
            name="DOWNLOAD MARKET OUTLOOK 2023"
            background="#EC6523"
            colorBorder="#B8501E"
            rounded
            uppercase
          />
        </div>
      </Container>
    </div>
  );
}
