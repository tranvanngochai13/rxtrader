import Image from "next/image";
import React from "react";
import { Container } from "../../common/Container";

const listShowrelated = [
  {
    id: 1,
    image: "/images/NEPCON Event/NEV_Showrelated-1.png",
  },
  {
    id: 2,
    image: "/images/NEPCON Event/NEV_Showrelated-2.png",
  },
  {
    id: 3,
    image: "/images/NEPCON Event/NEV_Showrelated-3.png",
  },
];

const listInfo = [
  {
    id: 1,
    image: "/images/NEPCON Event/ex.png",
  },
  {
    id: 2,
    image: "/images/NEPCON Event/vis.png",
  },
  {
    id: 3,
    image: "/images/NEPCON Event/conf.png",
  },
];

export default function AroundTheWorld() {
  return (
    <div
      style={{
        backgroundImage: `url("images/NEPCON Event/NEPCON Around The World.png")`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
      }}
      className="pt-[2.875rem] pb-[3.125rem]"
    >
      <Container>
        <div className="flex flex-col items-center">
          <h2 className="text-[2.5rem] text-[#E25520] font-bold leading-[130%] mb-[1.875rem]">
            Visit NEPCON series around the world
          </h2>

          <div className="grid grid-cols-3 gap-[1.375rem] mb-[5.211rem]">
            {listShowrelated.map((item) => {
              return (
                <div className="w-full h-auto shrink-0 bg-white">
                  <Image
                    src={item.image}
                    alt="image"
                    width={364}
                    height={111.63}
                    style={{
                      width: "100%",
                      height: "auto",
                      objectFit: "cover",
                    }}
                  />
                </div>
              );
            })}
          </div>

          <div className="grid grid-cols-3 gap-4">
            {listInfo.map((item) => {
              return (
                <div className="w-full h-auto shrink-0 bg-white">
                  <Image
                    src={item.image}
                    alt="image"
                    width={368}
                    height={243}
                    style={{
                      width: "100%",
                      height: "auto",
                      objectFit: "cover",
                    }}
                  />
                </div>
              );
            })}
          </div>
        </div>
      </Container>
    </div>
  );
}
