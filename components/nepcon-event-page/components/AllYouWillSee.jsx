import React from "react";
import {
  ExhibitorRegistrationIcon,
  VisitorRegistrationIcon,
} from "../../../assets/svg";
import { Button } from "../../common/Button";
import { Container } from "../../common/Container";
import { LineTitle } from "../../common/vme-expo-show/LineTitle";

export default function AllYouWillSee() {
  return (
    <div className="py-10">
      <Container>
        <div className="flex flex-col items-center gap-[1.875rem] text-center">
          <LineTitle
            title="All you will see at NEPCON Vietnam 2023"
            colorLine="#CD1719"
            Weight="bold"
          />
          <p className="text-[1.188rem] leading-[153%]">
            NEPCON Vietnam is the only and leading electronic - SMT exhibition
            in Vietnam where industrialists, manufacturers, policy makers,
            sellers, and buyers can find all supplies, updated technologies for
            parts, components, electronics, SMT, Handling and supporting
            industries with a premium free-to-attend business-to-business
            conference and networking event, hosted alongside an impressive
            exhibition. Featuring over 300 brands in Asia region, spectacular
            showcases over 35 hours of free-to-attend content and attracts over
            5,000 industrialist management and engineer professionals in 3 days.
            NEPCON is the ultimate platform for exclusive access to the latest
            technology, information, and trends to hear from industry experts
            and network with your peers.
          </p>
          <div className="flex items-center justify-between gap-4">
            <Button
              name="Exhibiting Information"
              icon={<ExhibitorRegistrationIcon />}
              background="#CD1719"
              colorBorder="#9C1415"
              rounded
            />
            <Button
              name="Visiting Information"
              icon={<VisitorRegistrationIcon />}
              background="#EA921F"
              colorBorder="#A56D17"
              rounded
            />
          </div>
        </div>
      </Container>
    </div>
  );
}
