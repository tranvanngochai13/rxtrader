import React from "react";
import Banner from "./components/Banner";
import EventInformation from "./components/EventInformation";
import AroundTheWorld from "./components/AroundTheWorld";
import AllYouWillSee from "./components/AllYouWillSee";
import Opportunities from "./components/Opportunities";
import AcitonBanner from "./components/AcitonBanner";
import MediaPartner from "./components/MediaPartner";
import MedicalSupporter from "./components/MedicalSupporter";
import AboutRXTradex from "../about-rxtradex/AboutRXTradex";
import ContactUs from "../contact-us/ContactUs";
import FollowUs from "../follow-us/FollowUs";
import ImportantNote from "../important-note/ImportantNote";
import ShowLocation from "../show-location/ShowLocation";
import AdsSection from "../ads-section/AdsSection";

export default function NEPCONEvent() {
  const content =
    "RX Tradex is ASEAN's leading exhibition organizer. We are a member of RX, the world’s leading events organizer who creates high profile, targeted industry events where buyers and suppliers come together to do business. Our global portfolio includes over 400 events serving 43 industries in 22 countries. In Thailand and Vietnam, we organize 20 strong brands of international exhibitions and conferences.";
  const backGround = "/images/NEPCON Event/Contact Information.png";
  const importantNote = `“NEPCON Vietnam" is open to trade visitors only. Please dress in business attire. Those wearing shorts and/or sandals and minors under the age of 15 will not be permitted into the exhibition hall. The organizer reserves the right to refuse admission to anyone without cause or explanation.`;

  return (
    <>
      <Banner />
      <AdsSection backgroundColor="#C9C9C9" />
      <EventInformation />
      <AroundTheWorld />
      <AllYouWillSee />
      <Opportunities />
      <AcitonBanner />
      <MediaPartner />
      <MedicalSupporter />
      <AboutRXTradex content={content} />
      <ContactUs backGround={backGround} />
      <ShowLocation
        map="/images/NEPCON Event/map.png"
        widthImage={1136}
        heightImage={454.13}
        weightTitle="bold"
      />
      <FollowUs />
      <ImportantNote backGround="#EFEFEF" importantNote={importantNote} />
    </>
  );
}
