import React from "react";
import { Container } from "../common/Container";

export default function ImportantNote({
  importantNote,
  backGround = "#EFEFEF",
}) {
  return (
    <div style={{ backgroundColor: `${backGround}` }} className="py-10">
      <Container>
        <div className="text-center flex flex-col items-center gap-[1.875rem]">
          <h2 className="text-[2.188rem] font-bold leading-[120%]">
            Important Note
          </h2>
          <p className="text-[1.063rem] leading-[151%]">{importantNote}</p>
        </div>
      </Container>
    </div>
  );
}
