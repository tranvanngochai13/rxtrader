import React from "react";
import { LineTitle } from "../../common/news-page/LineTitle";

export default function Hottest() {
  return (
    <div className="flex flex-col gap-[1.25rem]">
      <LineTitle className="pl-[0.625rem]">Hottest</LineTitle>
      <div className="flex flex-col gap-[1.875rem]">
        {[1, 2, 3].map((item) => {
          return (
            <div key={item} className="flex items-center gap-[0.938rem]">
              <div className="shrink-0 w-[3.125rem] h-[3.125rem] bg-DarkOrange rounded-full flex justify-center items-center text-white text-[0.938rem] leading-[108.5%] font-bold">
                {item}
              </div>
              <div>
                <h2 className="text-black text-[0.938rem] leading-[108.5%] font-bold mb-[0.375rem]">
                  Nepcon 2022: Creating opportunities for e-businesses
                </h2>
                <p className="text-Gray text-[0.813rem] leading-none font-medium italic">
                  Ho Chi Minh, hh:mm, dd/mm/yyyy
                </p>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
