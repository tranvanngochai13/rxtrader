import React from "react";
import { LineTitle } from "../../common/news-page/LineTitle";

const listCategory = [
  {
    id: 1,
    name: "Consu mer Electronic",
  },
  {
    id: 2,
    name: "Distributors in electronics industry ",
  },
  {
    id: 3,
    name: "Research & Development​",
  },
  {
    id: 4,
    name: "ASIAN",
  },
];

export default function Category() {
  return (
    <div className="flex flex-col gap-[0.625rem]">
      <LineTitle className="pl-[0.625rem]">Category</LineTitle>
      <ul>
        {listCategory.map((item) => {
          return (
            <li
              key={item.id}
              className="text-Gray text-[0.938rem] leading-[120%] p-[0.625rem] pl-[1.25rem]"
            >
              {item.name}
            </li>
          );
        })}
      </ul>
      <div className="text-Gray font-bold leading-none underline underline-offset-2">
        More Category
      </div>
    </div>
  );
}
