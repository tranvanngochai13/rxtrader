import Image from "next/image";
import React from "react";
import { LineTitle } from "../../common/news-page/LineTitle";

const listNews = [
  {
    id: 1,
    image: "/images/sidebar/new-1.png",
    tag: "Consumer Electronic",
    colorTag: "#CD1719",
    title: "Nepcon 2023: brings together nearly 300 electronic brands",
    date: "18:00, 17/02/2023",
    content:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain...",
  },
  {
    id: 2,
    image: "/images/sidebar/new-1.png",
    tag: "Research & Development",
    colorTag: "#428587",
    title:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain",
    date: "18:00, 17/02/2023",
    content:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain...",
  },
  {
    id: 3,
    image: "/images/sidebar/new-2.png",
    tag: "Distributors in Electronics Industry",
    colorTag: "#F26122",
    title:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain",
    date: "18:00, 17/02/2023",
    content:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain...",
  },
  {
    id: 4,
    image: "/images/sidebar/new-3.png",
    tag: "Electronic Components",
    colorTag: "#3EB247",
    title:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain",
    date: "18:00, 17/02/2023",
    content:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain...",
  },
];

export default function Newest() {
  return (
    <div className="flex flex-col gap-[1.25rem]">
      <LineTitle className="pl-[0.625rem]">Newest</LineTitle>

      <div className="flex flex-col gap-[0.625rem] pb-4 border-b border-Gray border-solid">
        <Image
          src="/images/sidebar/newest.png"
          alt="image"
          width={334}
          height={185}
          style={{ width: "100%", height: "auto", objectFit: "cover" }}
        />

        <div className="flex flex-col gap-[0.625rem]">
          <div
            className={`text-white leading-none font-semibold capitalize p-[0.313rem] max-w-fit bg-DarkOrange`}
          >
            Distributors in Electronics Industry
          </div>
          <h2 className="text-[1.25rem] text-black leading-[120%] font-bold">
            Nepcon 2022: Creating opportunities for e-businesses
          </h2>
          <p className="text-[#8B8B8B] leading-none font-medium italic">
            18:00, 17/02/2023
          </p>
        </div>
      </div>

      <div className="flex flex-col gap-[1.875rem]">
        {listNews.map((item) => {
          return (
            <div
              key={item.id}
              className="flex pb-4 border-b border-Gray border-solid"
            >
              <div className="w-[6.875rem] shrink-0">
                <Image
                  src={item.image}
                  alt="image"
                  width={110}
                  height={115}
                  style={{ width: "100%", height: "auto", objectFit: "cover" }}
                />
              </div>

              <div className="flex-1 flex flex-col justify-center p-[0.938rem] pr-0 gap-[0.625rem]">
                <div
                  className={`text-white text-[0.625rem] leading-none font-semibold capitalize p-[0.313rem] max-w-fit`}
                  style={{ backgroundColor: `${item.colorTag}` }}
                >
                  {item.tag}
                </div>
                <h2 className="text-[0.938rem] text-black leading-[108.5%] font-bold">
                  {item.title}
                </h2>
                <p className="text-[#8B8B8B] text-[0.813rem] leading-none font-medium italic">
                  {item.date}
                </p>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
