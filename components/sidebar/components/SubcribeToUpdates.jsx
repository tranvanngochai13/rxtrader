import React from "react";

export default function SubcribeToUpdates() {
  return (
    <div className="px-3">
      <div className="py-[1.25rem] px-[0.625rem] bg-white border border-solid border-[#8B8B8B] shadow-[3px_4px_16px_rgba(0,0,0,0.25)] rounded-[0.313rem] overflow-hidden">
        <div className="px-[0.625rem]">
          <form className="flex flex-col gap-[1.563rem] items-center">
            <h2 className="text-[1.75rem] text-black">Subcribe to Updates</h2>
            <p className="text-[#8B8B8B] text-[0.938rem] leading-[120%]">
              Get the latest news from RxTradex about all of our event
            </p>
            <input
              type="text"
              className="block w-full p-[0.625rem] text-center text-[0.938rem] font-medium italic text-[rgba(0,0,0,0.2)] bg-white border border-[#C9C9C9] rounded-[0.313rem] outline-none focus:outline-none"
              placeholder="Your Email Here"
            />
            <button className="py-2 px-[1.25rem] bg-DarkRed text-white text-[0.813rem] font-medium leading-[198%] uppercase border-b border-[#9C1415] border-solid shadow-[0px_3px_0px_#9C1415]">
              Subcribe
            </button>
            <div className="flex items-start">
              <input
                id="default-checkbox"
                type="checkbox"
                defaultValue
                className="w-4 h-4 bg-white rounded border-[1.5px] border-solid border-[#2D2D2D] outline-none focus:outline-none"
              />
              <label
                htmlFor="default-checkbox"
                className="ml-[0.625rem] text-sm leading-[120%]"
              >
                By Signing up, you agree to the our terms and our{" "}
                <span className="text-sm leading-[120%] font-bold">
                  Privacy Policy argreement
                </span>
              </label>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
