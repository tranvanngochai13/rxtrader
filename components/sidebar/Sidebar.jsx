import React from "react";
import Category from "./components/Category";
import Hottest from "./components/Hottest";
import Newest from "./components/Newest";
import SearchNews from "./components/SearchNews";
import SubcribeToUpdates from "./components/SubcribeToUpdates";

export default function Sidebar() {
  return (
    <div className="w-[30.755%] flex flex-col gap-[1.875rem]">
      <SearchNews />
      <Category />
      <Newest />
      <SubcribeToUpdates />
      <Hottest />
    </div>
  );
}
