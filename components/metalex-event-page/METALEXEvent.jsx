import React from "react";
import AboutTheShow from "./components/AboutTheShow";
import Activities from "./components/Activities";
import Banner from "./components/Banner";
import MediaPartner from "./components/MediaPartner";
import ReserveSpace from "./components/ReserveSpace";
import ScrollActivities from "./components/ScrollActivities";
import Statistical from "./components/Statistical";
import Testimonials from "./components/Testimonials";
import YoutubeEmbled from "./components/YoutubeEmbled";
import ContactUs from "../contact-us/ContactUs";
import FollowUs from "../follow-us/FollowUs";
import ImportantNote from "../important-note/ImportantNote";

export default function METALEXEvent() {
  const importantNote = `“METALEX Vietnam” is open to trade visitors only. Please dress in business attire. Those wearing shorts and/or sandals and minors under the age of 15 will not be permitted into the exhibition hall. The organizer reserves the right to refuse admission to anyone without cause or explanation.`;

  return (
    <>
      <Banner />
      <YoutubeEmbled />
      <Statistical />
      <AboutTheShow />
      <Activities />
      <Testimonials />
      <ScrollActivities />
      <MediaPartner />
      <ReserveSpace />
      <ContactUs
        backGround="transparent"
        textColor="#4E4D4D"
        Weight="bold"
        srcImage="/images/METALEXEvent/line.png"
      />
      <FollowUs />
      <ImportantNote backGround="#EFEFEF" importantNote={importantNote} />
    </>
  );
}
