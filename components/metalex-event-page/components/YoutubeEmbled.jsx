import React from "react";
import { Container } from "../../common/Container";

export default function YoutubeEmbled() {
  return (
    <div className="py-10">
      <Container>
        <div className="w-auto h-auto max-w-fit shrink-0 mx-auto">
          <iframe
            width="752"
            height="425"
            src="https://www.youtube.com/embed/3Imw4D358gI?controls=0"
            title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            allowfullscreen
          ></iframe>
        </div>
      </Container>
    </div>
  );
}
