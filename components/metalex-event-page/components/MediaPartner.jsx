import Image from "next/image";
import React from "react";
import { Container } from "../../common/Container";
import { LineTitle } from "../../common/vme-expo-show/LineTitle";

const listMeadia = [
  {
    id: 1,
    image: "/images/METALEXEvent/media-1.png",
  },
  {
    id: 2,
    image: "/images/METALEXEvent/media-2.png",
  },
  {
    id: 3,
    image: "/images/METALEXEvent/media-3.png",
  },
  {
    id: 4,
    image: "/images/METALEXEvent/media-4.png",
  },
  {
    id: 5,
    image: "/images/METALEXEvent/media-5.png",
  },
  {
    id: 6,
    image: "/images/METALEXEvent/media-6.png",
  },
  {
    id: 7,
    image: "/images/METALEXEvent/media-7.png",
  },
  {
    id: 8,
    image: "/images/METALEXEvent/media-8.png",
  },
  {
    id: 9,
    image: "/images/METALEXEvent/media-9.png",
  },
];

export default function MediaPartner() {
  return (
    <div className="py-10">
      <Container>
        <div className="flex flex-col gap-[1.875rem]">
          <LineTitle
            title="Media Partner"
            colorLine="#EC6523"
            textColor="black"
            Weight="bold"
          />
          <div className="flex justify-center flex-wrap gap-4">
            {listMeadia.map((item) => {
              return (
                <div key={item.id} className="h-[5.625rem] w-auto shrink-0">
                  <Image
                    src={item.image}
                    alt="image"
                    width={272}
                    height={90}
                    style={{
                      width: "100%",
                      height: "auto",
                      objectFit: "cover",
                    }}
                  />
                </div>
              );
            })}
          </div>
        </div>
      </Container>
    </div>
  );
}
