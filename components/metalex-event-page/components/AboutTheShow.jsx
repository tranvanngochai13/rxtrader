import Image from "next/image";
import React from "react";
import { Button } from "../../common/Button";
import { Container } from "../../common/Container";

export default function AboutTheShow() {
  return (
    <div
      style={{
        backgroundImage: `url("images/METALEXEvent/About the show.png")`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
      }}
      className="py-10"
    >
      <Container>
        <div className="flex justify-between gap-4">
          <div className="w-[49.464%] flex flex-col gap-[1.25rem]">
            <div>
              <h3 className="text-[1.875rem] italic uppercase leading-[79%] font-bold">
                Creating
              </h3>
              <h2 className="text-[3.75rem] text-black italic font-black leading-none tracking-[0.09em] drop-shadow-[0px_-1px_4px_rgba(0,0,0,0.25)]">
                THE NEXT GIANT
              </h2>
              <h3 className="text-[1.875rem] italic uppercase leading-[79%] font-bold">
                Manufacturers
              </h3>
            </div>
            <p className="text-[0.938rem] text-black leading-[160%]">
              METALEX Vietnam 2023 would be taking place under the theme of
              “CREATING THE NEXT GIANT MANUFACTURERS” with the aim of
              accelerating Vietnam’s manufacturing market to rebound. The event
              promises to offer a one-stop destination for all world-class
              technology providers and local industrialists to exchange
              breakthrough ideas, business know-how, along with advancements
              which could lead to a burst of innovation, productivity
              improvement, and a reconnected world. This would be achieved
              through various year-round activities such as technology
              showcases, skill competitions, factory roadshows,
              thought-provoking conferences, and business matchmaking events. ​
            </p>
            <Button
              name="Register Your Interest"
              background="#EC6523"
              colorBorder="#B8501E"
            />
          </div>
          <div className="w-[50.536%]">
            <Image
              src="/images/METALEXEvent/About the show-user.png"
              alt="image"
              width={334}
              height={185}
              style={{ width: "100%", height: "auto", objectFit: "cover" }}
            />
          </div>
        </div>
      </Container>
    </div>
  );
}
