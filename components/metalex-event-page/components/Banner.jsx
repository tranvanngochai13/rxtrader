import Image from "next/image";
import React from "react";
import { Button } from "../../common/Button";
import { Container } from "../../common/Container";

export default function Banner() {
  return (
    <div
      style={{
        backgroundImage: `url("images/METALEXEvent/METALEX Tittle Screen.png")`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
      }}
      className="py-[8.813rem] relative"
    >
      <Container>
        <div className="mx-auto max-w-[47rem] flex flex-col items-center gap-[1.875rem]">
          <h2 className="text-white text-[1.563rem] font-bold leading-[116%] text-center mb-[0.313rem]">
            Vietnam's lnternational Exhibition on Machine Tool & Metalworking
            SoIutions for Production Upgrade - 16 th Edition
          </h2>
          <Image
            src="/images/METALEXEvent/logo.png"
            alt="image"
            width={680}
            height={143}
            style={{ objectFit: "cover" }}
          />

          <div className="flex items-center justify-between gap-[0.938rem] text-[1.188rem] text-white leading-[171%] font-bold">
            <div className="flex items-center gap-[0.375rem]">
              <svg
                width="21"
                height="29"
                viewBox="0 0 21 29"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M17.9438 5.94922H15.975V3.81641C15.975 3.57031 15.7289 3.32422 15.4828 3.32422H13.8422C13.5551 3.32422 13.35 3.57031 13.35 3.81641V5.94922H8.1V3.81641C8.1 3.57031 7.85391 3.32422 7.60781 3.32422H5.96719C5.68008 3.32422 5.475 3.57031 5.475 3.81641V5.94922H3.50625C2.39883 5.94922 1.5375 6.85156 1.5375 7.91797V22.3555C1.5375 23.4629 2.39883 24.3242 3.50625 24.3242H17.9438C19.0102 24.3242 19.9125 23.4629 19.9125 22.3555V7.91797C19.9125 6.85156 19.0102 5.94922 17.9438 5.94922ZM17.6977 22.3555H3.75234C3.58828 22.3555 3.50625 22.2734 3.50625 22.1094V9.88672H17.9438V22.1094C17.9438 22.2734 17.8207 22.3555 17.6977 22.3555Z"
                  fill="#EC6523"
                />
              </svg>

              <span>04–06 October 2023</span>
            </div>
            <div className="flex items-center gap-[0.375rem]">
              <svg
                width="17"
                height="23"
                viewBox="0 0 17 23"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M7.67969 21.5801C8.04883 22.1543 8.91016 22.1543 9.2793 21.5801C15.2676 12.9668 16.375 12.0645 16.375 8.86523C16.375 4.51758 12.8477 0.990234 8.5 0.990234C4.11133 0.990234 0.625 4.51758 0.625 8.86523C0.625 12.0645 1.69141 12.9668 7.67969 21.5801ZM8.5 12.1465C6.6543 12.1465 5.21875 10.7109 5.21875 8.86523C5.21875 7.06055 6.6543 5.58398 8.5 5.58398C10.3047 5.58398 11.7812 7.06055 11.7812 8.86523C11.7812 10.7109 10.3047 12.1465 8.5 12.1465Z"
                  fill="#EC6523"
                />
              </svg>

              <span>S.E.C.C, HCMC</span>
            </div>
          </div>
          <div className="flex flex-col items-center gap-[1.25rem] w-full">
            <Button
              name="Reserve Space"
              background="#EC6523"
              colorBorder="#B8501E"
            />
            <Button
              name="Download Exhibitor Brochure"
              background="#EC6523"
              colorBorder="#B8501E"
            />
          </div>
        </div>
        <div className="absolute left-[50%] bottom-[0.784rem] translate-x-[-50%]">
          <svg
            width="24"
            height="15"
            viewBox="0 0 24 15"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M11.0332 14.0957C11.5254 14.5879 12.3457 14.5879 12.8379 14.0957L23.502 3.48633C23.9941 2.93945 23.9941 2.11914 23.502 1.62695L22.2441 0.369141C21.752 -0.123047 20.9316 -0.123047 20.3848 0.369141L11.9629 8.79102L3.48633 0.369141C2.93945 -0.123047 2.11914 -0.123047 1.62695 0.369141L0.369141 1.62695C-0.123047 2.11914 -0.123047 2.93945 0.369141 3.48633L11.0332 14.0957Z"
              fill="white"
            />
          </svg>
        </div>
      </Container>
    </div>
  );
}
