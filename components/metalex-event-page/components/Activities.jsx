import Image from "next/image";
import React from "react";
import { Button } from "../../common/Button";
import { Container } from "../../common/Container";
import { LineTitle } from "../../common/vme-expo-show/LineTitle";

const listBody = [
  {
    id: 1,
    title: "Exhibiting",
    desc: "MXV23 presents the ideal opportunity to showcase your products, technologies, innovations and solutions to the metalworking community.​ Participate in this important international meeting and discover all the opportunities that MXV 2023 can offer you​",
    textbutton: "Discovery More",
    image: "/images/METALEXEvent/Activities-1.png",
  },
  {
    id: 2,
    title: "Private Networking Events​",
    desc: "The Private Networking Events M-TALKS is the convening platform to engage in debate and thought leadership, share ideas and solutions to drive towards the strategies for the future.​​​",
    textbutton: "Mtalk",
    image: "/images/METALEXEvent/Activities-2.png",
  },
  {
    id: 3,
    title: "Sponsoring",
    desc: "Ensure your brand is at the forefront of metalworking community by taking a sponsorship at MXV23 which will provide you exposure before, during and after the event.​​",
    textbutton: "Discovery More",
    image: "/images/METALEXEvent/Activities-3.png",
  },
];

export default function Activities() {
  return (
    <div className="py-10">
      <Container>
        <div className="flex flex-col gap-10">
          <LineTitle
            title="METALEX 2023 is the convening platform for the Metalworking and Machine Tools sector"
            colorLine="#EC6523"
            textColor="black"
            Weight="bold"
            className="px-[5.688rem]"
          />
          <div className="flex flex-col gap-10">
            {listBody.map((item) => {
              return (
                <div className="flex items-center gap-4">
                  <div className={`shrink-0 ${item.id == 2 ? "order-2" : ""}`}>
                    <Image
                      src={item.image}
                      alt="image"
                      width={560}
                      height={372}
                      style={{ objectFit: "cover" }}
                    />
                  </div>
                  <div className="flex flex-col items-center gap-6 text-center">
                    <h3 className="text-black text-[1.75rem] font-bold leading-[117.857%]">
                      {item.title}
                    </h3>
                    <p className="text-black text-[1.063rem] leading-[171%]">
                      {item.desc}
                    </p>
                    <Button
                      name={item.textbutton}
                      background="#EC6523"
                      colorBorder="#B8501E"
                    />
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </Container>
    </div>
  );
}
