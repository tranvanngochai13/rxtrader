import Image from "next/image";
import React from "react";
import { Button } from "../../common/Button";
import { Container } from "../../common/Container";

const listScroll = [
  {
    id: 1,
    image: "/images/METALEXEvent/scroll-1.png",
  },
  {
    id: 2,
    image: "/images/METALEXEvent/scroll-2.png",
  },
  {
    id: 3,
    image: "/images/METALEXEvent/scroll-3.png",
  },
  {
    id: 4,
    image: "/images/METALEXEvent/scroll-4.png",
  },
];

export default function ScrollActivities() {
  return (
    <div className="py-10">
      <Container>
        <div className="flex flex-col items-center gap-[1.875rem] text-center">
          <h2 className="text-[2.5rem] font-bold leading-[130%] uppercase">
            AN EXCLUSIVE SERIES OF ACTIVITIES FOR THE METALWORKING COMMUNITY OF
            METALEX VIETNAM 2023
          </h2>
          <p className="text-[1.063rem] leading-[171%]">
            Take a deep dive into the newest trends from regulations and
            requirement to expert analysis and trend reports, find out the
            latest predictions and use it to inform your strategies for the
            future.
          </p>
          <div>
            <div className="grid grid-cols-4 gap-4">
              {listScroll.map((item) => {
                return (
                  <div key={item.id} className="w-auto h-auto shrink-0">
                    <Image
                      src={item.image}
                      alt="image"
                      width={272}
                      height={145}
                      style={{
                        width: "100%",
                        height: "auto",
                        objectFit: "cover",
                      }}
                    />
                  </div>
                );
              })}
            </div>
            <span className="mt-[1.188rem] text-[0.938rem] leading-[171%] block mx-auto">
              4 / 4
            </span>
          </div>
          <Button name="Read More" background="#EC6523" colorBorder="#B8501E" />
        </div>
      </Container>
    </div>
  );
}
