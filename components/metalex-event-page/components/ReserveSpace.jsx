import React from "react";
import { Button } from "../../common/Button";
import { Container } from "../../common/Container";
import { LineTitle } from "../../common/vme-expo-show/LineTitle";

export default function ReserveSpace() {
  return (
    <div
      style={{
        backgroundImage: `url("images/METALEXEvent/Reserve Space.png")`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
      }}
      className="py-10"
    >
      <Container>
        <div className="flex flex-col gap-[1.875rem] max-w-[47rem] mx-auto">
          <LineTitle
            title="Reserve Your Space Now!"
            colorLine="#EC6523"
            textColor="black"
            Weight="bold"
          />
          <div className="flex justify-between">
            <Button
              name="Book Your Stand"
              background="#333333"
              colorBorder="black"
            />
            <Button
              name="Download the Brochure"
              background="#333333"
              colorBorder="black"
            />
          </div>
        </div>
      </Container>
    </div>
  );
}
