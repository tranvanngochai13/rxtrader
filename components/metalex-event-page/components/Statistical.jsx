import Image from "next/image";
import React from "react";
import { Container } from "../../common/Container";

export default function Statistical() {
  return (
    <div>
      <Container>
        <Image
          src="/images/METALEXEvent/statistical.png"
          alt="image"
          width={1136}
          height={621}
          style={{ width: "100%", height: "auto", objectFit: "cover" }}
        />
      </Container>
    </div>
  );
}
