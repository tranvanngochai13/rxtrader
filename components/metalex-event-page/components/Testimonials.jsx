import Image from "next/image";
import React from "react";
import { Container } from "../../common/Container";
import { LineTitle } from "../../common/vme-expo-show/LineTitle";

const listTestimonials = [
  {
    id: 1,
    content:
      "“Nice location. I'm very happy that the exhibition is well prepared, the booth setup is as beautiful as it was 2 years ago before the epidemic. There have been some visitors to the booth and were also the right audience for th the woiejowv company.”",
    image: "/images/METALEXEvent/Testimonials-1.png",
    name: "Mr Yoshimoto Makoto",
    desc: "Director  | Yoshimoto Vietnam Company Limited",
  },
  {
    id: 2,
    content:
      "“The setup was really great. It was really easy. We're all very happy with our booth.”",
    image: "/images/METALEXEvent/Testimonials-2.png",
    name: "Ms. Nicole Glogner",
    desc: "Project Manager Fair Consulting | Bavaria Pavilion",
  },
];

export default function Testimonials() {
  return (
    <div
      style={{
        backgroundImage: `url("images/METALEXEvent/bg-Testimonials.png")`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
      }}
      className="pt-10 pb-[8.125rem]"
    >
      <Container>
        <div className="flex flex-col gap-10">
          <LineTitle
            title="Testimonials"
            colorLine="#EC6523"
            Weight="bold"
            textColor="white"
          />
          <div className="flex gap-4 px-[6rem]">
            {listTestimonials.map((item) => {
              return (
                <div
                  key={item.id}
                  className="w-1/2 flex flex-col gap-[1.875rem]"
                >
                  <p className="text-center text-white text-lg leading-[185%] font-bold">
                    {item.content}
                  </p>
                  <div className="mt-auto text-center flex flex-col items-center gap-[0.188rem]">
                    <Image
                      src={item.image}
                      alt="image"
                      width={80}
                      height={80}
                      style={{ objectFit: "cover" }}
                      className="rounded-full overflow-hidden"
                    />
                    <p className="text-white text-base leading-[150%]">
                      {item.name}
                    </p>
                    <p className="text-white text-base leading-[120%]">
                      {item.desc}
                    </p>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </Container>
    </div>
  );
}
