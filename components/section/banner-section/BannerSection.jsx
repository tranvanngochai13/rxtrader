import React from "react";
import { useRef } from "react";
import { useEffect } from "react";

export default function BannerSection() {
  const vidRef = useRef();

  useEffect(() => {
    vidRef.current.play();
  }, []);

  return (
    <div className="h-auto w-full">
      <video
        src="/videos/Reed Tradex is Now -RX Tradex-.mp4"
        className="w-full h-full"
        ref={vidRef}
        muted
        autoPlay
        loop
      />
    </div>
  );
}
