import Image from "next/image";
import React from "react";
import { TitleHomePage } from "../../common/home-page/TitleHomePage";
import { Container } from "../../common/Container";

export default function InfoSection() {
  return (
    <div className="bg-GhostWhite ">
      <Container>
        <div className="flex flex-col lg:flex-row gap-[3.438rem] py-10">
          <Image
            src="/images/Home/InfoSection/info.png"
            alt=""
            width={522}
            height={317}
            style={{ objectFit: "cover", width: "100%", height: "auto" }}
          />
          <div className="flex flex-col gap-[0.625rem]">
            <TitleHomePage>
              <span className="text-secondary mr-1">RX Tradex :</span>
              <span>Kết nối phát triển doanh nghiệp</span>
            </TitleHomePage>

            <p className="text-BlackText text-[0.938rem]">
              Là thành viên của RX, một trong những đơn vị tổ chức sự kiện hàng
              đầu thế giới có trụ sở đặt tại London, Anh. RX Tradex (tên gọi cũ
              là Reed Tradex) tại Bangkok, Thái Lan, đơn vị hàng đầu về tổ chức
              các sự kiện cao. Đồng thời, việc sở hữu cơ sở dữ liện và khả năng
              số hóa bằng công nghệ cũng giúp tạo ra nhiều hoạt động thường niên
              và xuyên suốt, góp phần hỗ trợ các doanh nghiệp trong việc phát
              triển kinh doanh và khách hàng của họ. Với hình ảnh nhận diện
              thương hiệu mới nhằm thể hiện quá trình chuyển đổi kỹ thuật số của
              chúng tôi, mang tinh thần phục vụ khách hàng 24/7, 365 ngày và
              xuyên suốt các ngày sự kiện.
            </p>
            <button className="py-[0.938rem] px-[1.563rem] bg-white border border-DarkBlack border-solid rounded-[0.188rem] max-w-fit text-[#333333] leading-none">
              Xem thêm
            </button>
          </div>
        </div>
      </Container>
    </div>
  );
}
