import Image from "next/image";
import React from "react";
import { Container } from "../../common/Container";
import { TitleHomePage } from "../../common/home-page/TitleHomePage";

const listEvent = [
  {
    id: 1,
    images: [
      {
        id: 1,
        image: "/images/Home/ExhibitionSection/vme.png",
      },
    ],
    title: "Vietnam Manufacturing Expo",
    address: "Hà Nội, Việt Nam",
    date: "Ngày 09 - 11 Tháng 08 Năm 2023",
    time: "09:00 - 17:00 hrs.",
    content: "Cung Văn Ho á Hữu nghị Việt Xô (ICE)",
  },
  {
    id: 2,
    images: [
      {
        id: 1,
        image: "/images/Home/ExhibitionSection/nepcon.png",
      },
    ],
    title: "NEPCON Vietnam",
    address: "Hà Nội, Việt Nam",
    date: "Ngày 06 - 08 Tháng 09 Năm 2023",
    time: "09:00 - 17:00 hrs.",
    content: "Cung Văn Hoá Hữu nghị Việt Xô (ICE)",
  },
  {
    id: 3,
    images: [
      {
        id: 1,
        image: "/images/Home/ExhibitionSection/metalex.png",
      },
    ],
    title: "METALEX Vietnam",
    address: "Hồ Chí Minh, Việt Nam",
    date: "Ngày 04 - 06 Tháng 10 Năm 2023",
    time: "??:00 - ??:00 hrs.",
    content: "????????????????????????????",
  },
  {
    id: 4,
    images: [
      {
        id: 1,
        image: "/images/Home/ExhibitionSection/metalex-1.png",
      },
      {
        id: 2,
        image: "/images/Home/ExhibitionSection/nepcon.png",
      },
    ],
    title: "METALEX Vietnam & NEPCON Vietnam",
    address: "Hồ Chí Minh, Việt Nam",
    date: "Ngày 04 - 06 Tháng 10 Năm 2023",
    time: "09:00 - 17:00 hrs.",
    content: "Trung tâm Hội chợ & Triển lãm SECC",
  },
  {
    id: 5,
    images: [
      {
        id: 1,
        image: "/images/Home/ExhibitionSection/metalex.png",
      },
    ],
    title: "Waste & Recycling Expo Vietnam",
    address: "Hồ Chí Minh, Việt Nam",
    date: "Ngày 09 - 11 Tháng 08 Năm 2023",
    time: "??:?? - ??:?? hrs.",
    content: "?????????????????????????????????",
  },
];

export default function ExhibitionSection({ registrationForm = false }) {
  return (
    <div className="py-10">
      <Container>
        {registrationForm ? (
          <TitleHomePage className="mb-10">
            <span>Các triển lãm khác</span>
          </TitleHomePage>
        ) : (
          <TitleHomePage className="mb-10">
            <span>Triển Lãm</span>
            <span className="text-secondary ml-1">Tiếp Theo</span>
          </TitleHomePage>
        )}

        <div className="grid grid-cols-1 lg:grid-cols-2 gap-[0.938rem]">
          {listEvent.map((item, index) => {
            return (
              <div key={index} className="w-full">
                <div className="relative flex">
                  <div className="w-[34.164%] shrink-0 flex flex-col justify-center items-center gap-[0.625rem] bg-DarkGray">
                    {item.images.map((logo) => {
                      return (
                        <Image
                          key={logo.id}
                          src={logo.image}
                          alt="image"
                          width={144}
                          height={31}
                          style={{
                            objectFit: "cover",
                            width: "auto",
                            height: "auto",
                          }}
                        />
                      );
                    })}
                  </div>
                  <div className="w-[65.836%] flex pl-5 bg-DimGray text-DarkBlack overflow-hidden">
                    <div className="z-10 py-[0.844rem]">
                      <div className="flex flex-col gap-[0.188rem] mb-3">
                        <h3 className="text-base font-bold leading-[120%]">
                          {item.title}
                        </h3>
                        <p className="text-[0.988rem] leading-[120%]">
                          {item.address}
                        </p>
                        <div className="py-[0.625rem] w-[72.4138%]">
                          <span className="block bg-DarkBlack h-[2px] w-full"></span>
                        </div>
                      </div>
                      <div className="text-[0.911rem] leading-[120%]">
                        <p className="font-bold">{item.date}</p>
                        <p className="font-medium">{item.time}</p>
                        <p className="font-semibold">{item.content}</p>
                      </div>
                    </div>
                  </div>
                  <div className="absolute right-0 top-0 bottom-0">
                    <Image
                      src="/images/Home/ExhibitionSection/bg.png"
                      alt="image"
                      width={60}
                      height={160}
                      style={{
                        objectFit: "cover",
                        height: "100%",
                        flexShrink: 0,
                      }}
                    />
                  </div>
                </div>
                <div className="bg-LightGrey flex justify-end items-center py-[0.219rem] px-[0.625rem]">
                  <Image
                    src="/images/Home/ExhibitionSection/bottom-bg.png"
                    alt=""
                    width={93.91}
                    height={99.14}
                    style={{ objectFit: "cover", height: "auto" }}
                  />
                </div>
              </div>
            );
          })}
        </div>

        <div className="bg-RedLoadMore text-white text-[0.781rem] font-semibold leading-[120%] py-[0.438rem] px-10 max-w-fit mt-10 mb-[0.625rem] mx-auto">
          Xem Danh sách tất cả Lịch triển lãm
        </div>
        <div className="max-w-[59.25rem] mx-auto">
          <Image
            src="/images/Home/ExhibitionSection/banner.png"
            alt=""
            width={948}
            height={113.56}
            style={{ objectFit: "cover", height: "auto", width: "100%" }}
          />
        </div>
      </Container>
    </div>
  );
}
