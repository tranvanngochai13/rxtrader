import React from "react";

export function Button({
  name,
  icon,
  background,
  colorBorder,
  rounded = false,
  uppercase = false,
}) {
  return (
    <div
      style={{
        backgroundColor: `${background}`,
        border: `1px solid ${colorBorder}`,
        boxShadow: `0px 3px 0px ${colorBorder}`,
      }}
      className={`flex items-center gap-[0.625rem] py-2 px-[1.25rem] max-w-fit ${
        rounded ? "rounded-[0.438rem]" : ""
      }`}
    >
      {icon}
      <span
        className={`text-white text-[0.938rem] font-medium leading-[171%] ${
          uppercase ? "uppercase" : ""
        }`}
      >
        {name}
      </span>
    </div>
  );
}
