import clsx from "clsx";

export function Container({ className, ...props }) {
  return (
    <div
      className={clsx("mx-auto max-w-[71rem] w-full px-4 xl:px-0", className)}
      {...props}
    />
  );
}
