import clsx from "clsx";

export function TitleHomePage({ className, ...props }) {
  return (
    <h2
      className={clsx(
        "text-BlackText text-[1.875rem] leading-[120%] font-semibold",
        className
      )}
      {...props}
    />
  );
}
