import clsx from "clsx";

export function LineTitle({ className, ...props }) {
  return (
    <div className="pt-[0.625rem] pb-[0.563rem] border-b border-Gray border-solid">
      <h2
        className={clsx(
          "text-black text-[1.188rem] leading-[171%] font-bold max-w-fit relative after:absolute after:content-[''] after:left-0 after:bottom-[-0.625rem] after:bg-DarkRed after:h-[0.188rem] after:w-full",
          className
        )}
        {...props}
      />
    </div>
  );
}
