import clsx from "clsx";

export function LineTitle({
  className,
  title,
  colorLine,
  textColor = "DarkBlack",
  Weight = "normal",
}) {
  return (
    <div className="flex flex-col items-center">
      <h2
        style={{ color: `${textColor}`, fontWeight: `${Weight}` }}
        className={clsx(
          `text-center text-[2.5rem] leading-[117.5%] mb-[1.25rem]`,
          className
        )}
      >
        {title}
      </h2>
      <div
        style={{ backgroundColor: `${colorLine}` }}
        className="w-[10.25rem] h-[0.375rem] mx-auto"
      ></div>
    </div>
  );
}
