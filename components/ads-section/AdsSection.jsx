import Image from "next/image";
import React from "react";
import { Container } from "../common/Container";

export default function AdsSection({ backgroundColor = "#C9C9C9" }) {
  return (
    <div style={{ backgroundColor: `${backgroundColor}` }} className="py-10">
      <Container>
        <div className="flex gap-4">
          <Image
            src="/images/news-page/ads-1.png"
            alt="image"
            width={562}
            height={163}
            style={{ objectFit: "cover" }}
          />
          <Image
            src="/images/news-page/ads-2.png"
            alt="image"
            width={562}
            height={163}
            style={{ objectFit: "cover" }}
          />
        </div>
      </Container>
    </div>
  );
}
