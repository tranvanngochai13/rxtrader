import Image from "next/image";
import React from "react";
import { Container } from "../common/Container";

const listContact = [
  {
    id: 1,
    title: "To Reserve Space",
    content: [
      { id: 1, name: "T: (+84) 286 287 3355 (Vietnam)" },
      { id: 2, name: "T: (+66) 2686 7299 (Thailand)" },
      { id: 3, name: "E. ", mail: "nepconvietnam@rxtradex.com" },
    ],
  },
  {
    id: 2,
    title: "For Visitors",
    content: [
      { id: 1, name: "T: (+84) 286 287 3355 (Vietnam)" },
      { id: 2, name: "T: (+66) 2686 7299 (Thailand)" },
      { id: 3, name: "E. ", mail: "contactcenter@rxtradex.com" },
    ],
  },
];

export default function ContactUs({
  backGround,
  textColor = "#FFFFFF",
  Weight,
  srcImage = "/images/VMEExpoShow/line.png",
}) {
  return (
    <div
      style={{
        backgroundImage: `url("${backGround}")`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
      }}
      className="py-[2.5rem]"
    >
      <Container>
        <div
          style={{ color: `${textColor}` }}
          className={`flex flex-col items-center gap-[1.875rem] text-center`}
        >
          <h2 className={`text-[2.5rem] leading-[130%] font-${Weight}`}>
            Contact Us
          </h2>
          <div className="flex justify-between gap-[4rem] pl-[6rem] w-full">
            {listContact.map((item) => {
              return (
                <div
                  key={item.id}
                  className="min-w-[17rem] flex flex-col gap-[1.25rem]"
                >
                  <h3 className="text-[1.75rem] leading-[117.86%] font-bold">
                    {item.title}
                  </h3>
                  <p className="text-[1.063rem] leading-[171%] font-bold">
                    Please contact Call Center :
                  </p>
                  <ul className="text-[1.063rem] leading-[200%]">
                    {item.content.map((text) => {
                      return (
                        <React.Fragment key={text.id}>
                          <li>
                            {text.name}{" "}
                            <span className="text-[#EC6523]">{text?.mail}</span>
                          </li>
                        </React.Fragment>
                      );
                    })}
                  </ul>
                </div>
              );
            })}

            <div className="flex flex-col gap-[1.25rem]">
              <h3 className="text-[1.75rem] leading-[135.7%] font-bold">
                For Media Partner and To join BIZ - Business Innovation Zone
                Project
              </h3>
              <p className="text-[1.063rem] leading-[171%] font-bold">
                Please contact : Lê Thị Kim Thơ (Sophie)
              </p>
              <ul className="text-[1.063rem] leading-[200%]">
                <li>T: (+84) 28 6287 3355 - Ext 115</li>
                <li>M: (+84) 937 77 2297</li>
                <li>
                  E. <span className="text-[#EC6523]">tho.le@rxtradex.com</span>
                </li>
              </ul>
            </div>
          </div>
          <Image
            src={srcImage}
            alt="image"
            width={1136}
            height={63}
            style={{ width: "100%", height: "auto", objectFit: "cover" }}
          />
        </div>
      </Container>
    </div>
  );
}
