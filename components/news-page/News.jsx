import React from "react";
import ListNews from "./components/ListNews";
import SpotlightNews from "./components/SpotlightNews";

export default function News() {
  return (
    <div className="w-[69.245%]">
      <SpotlightNews />
      <ListNews />
    </div>
  );
}
