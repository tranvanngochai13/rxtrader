import Image from "next/image";
import React from "react";
import { LoadMoreIcon } from "../../../assets/svg";
import { LineTitle } from "../../common/news-page/LineTitle";

const listNews = [
  {
    id: 1,
    image: "/images/news-page/new-1.png",
    tag: "Consumer Electronic",
    colorTag: "#CD1719",
    title:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain",
    date: "18:00, 17/02/2023",
    content:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain...",
  },
  {
    id: 2,
    image: "/images/news-page/new-1.png",
    tag: "Research & Development",
    colorTag: "#428587",
    title:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain",
    date: "18:00, 17/02/2023",
    content:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain...",
  },
  {
    id: 3,
    image: "/images/news-page/new-1.png",
    tag: "Distributors in Electronics Industry",
    colorTag: "#F26122",
    title:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain",
    date: "18:00, 17/02/2023",
    content:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain...",
  },
  {
    id: 4,
    image: "/images/news-page/new-1.png",
    tag: "Electronic Components",
    colorTag: "#3EB247",
    title:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain",
    date: "18:00, 17/02/2023",
    content:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain...",
  },
  {
    id: 5,
    image: "/images/news-page/new-1.png",
    tag: "Consumer Electronic",
    colorTag: "#CD1719",
    title:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain",
    date: "18:00, 17/02/2023",
    content:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain...",
  },
  {
    id: 6,
    image: "/images/news-page/new-1.png",
    tag: "Consumer Electronic",
    colorTag: "#CD1719",
    title:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain",
    date: "18:00, 17/02/2023",
    content:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain...",
  },
  {
    id: 7,
    image: "/images/news-page/new-1.png",
    tag: "Research & Development",
    colorTag: "#428587",
    title:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain",
    date: "18:00, 17/02/2023",
    content:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain...",
  },
  {
    id: 8,
    image: "/images/news-page/new-1.png",
    tag: "Distributors in Electronics Industry",
    colorTag: "#F26122",
    title:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain",
    date: "18:00, 17/02/2023",
    content:
      "Nepcon 2022: Creating opportunities for e-businesses to join the global supply chain...",
  },
];

export default function ListNews() {
  return (
    <div className="flex flex-col gap-[1.875rem]">
      <div>
        <LineTitle>Distributors in Electronics Industry</LineTitle>
      </div>
      <div className="flex flex-col gap-[1.875rem]">
        {listNews.map((item) => {
          return (
            <div key={item.id} className="flex gap-4">
              <div className="w-[17rem] h-[11.938rem] shrink-0">
                <Image
                  src={item.image}
                  alt="image"
                  width={272}
                  height={191}
                  style={{ objectFit: "cover" }}
                  className="shrink-0 w-full h-full"
                />
              </div>

              <div className="flex-1 flex flex-col gap-3">
                <div className="flex flex-col gap-3">
                  <div
                    className={`text-white leading-none font-semibold capitalize p-[0.313rem] max-w-fit`}
                    style={{ backgroundColor: `${item.colorTag}` }}
                  >
                    {item.tag}
                  </div>
                  <h2 className="text-[1.25rem] text-black leading-[120%] font-bold">
                    {item.title}
                  </h2>
                  <p className="text-[#8B8B8B] leading-none font-medium italic">
                    {item.date}
                  </p>
                  <p className="text-[1.063rem] text-Gray leading-[120%]">
                    {item.content}
                  </p>
                </div>
                <button className="text-Gray text-sm font-bold leading-none max-w-fit border-b border-solid border-Gray mt-auto shrink-0">
                  Xem thêm
                </button>
              </div>
            </div>
          );
        })}
      </div>

      <div className="flex items-center gap-[0.625rem] p-[0.625rem] border border-[##8B8B8B] border-solid max-w-fit mx-auto">
        <LoadMoreIcon />
        <span className="text-[#8B8B8B] text-sm leading-none cursor-pointer">
          Load more
        </span>
      </div>
    </div>
  );
}
