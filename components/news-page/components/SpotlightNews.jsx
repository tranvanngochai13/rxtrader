import Image from "next/image";
import React from "react";

export default function SpotlightNews() {
  return (
    <div className="mb-[1.875rem] relative after:absolute after:content-[''] after:bg-black after:opacity-50 after:top-0 after:left-0 after:bottom-0 after:right-0">
      <Image
        src="/images/news-page/Spotlight News.png"
        alt="image"
        width={752}
        height={400}
        style={{ width: "100%", height: "auto", objectFit: "cover" }}
      />
      <div className="absolute left-[1.563rem] bottom-[1.563rem] z-10 flex flex-col gap-[0.938rem]">
        <div className="text-white leading-none font-semibold capitalize p-[0.313rem] bg-DarkOrange max-w-fit">
          Distributors in Electronics Industry
        </div>
        <h2 className="text-[2.188rem] text-white leading-[120%] font-bold drop-shadow-[0px_4px_4px_rgba(0,0,0,0.25)]">
          Reed Tradex team visited Thai Garment Manufacturers Association
        </h2>
        <p className="text-GhostWhite leading-none font-medium italic">
          18:00, 17/02/2023 - 134 views
        </p>
      </div>
    </div>
  );
}
