import Image from "next/image";
import React from "react";
import { Container } from "../common/Container";

const listNews = [
  {
    id: 1,
    tag: "Consumer Electronic",
    colorTag: "#CD1719",
    title: "Nepcon 2022: Creating opportunities for supporting industry",
    date: "18:00, 17/02/2023",
  },
  {
    id: 2,
    tag: "Electronic Components",
    colorTag: "#3EB247",
    title: "Towards smart production for Vietnamese supporting industry ",
    date: "18:00, 17/02/2023",
  },
  {
    id: 3,
    tag: "Distributors in Electronics Industry",
    colorTag: "#F26122",
    title: "Nepcon 2022: Creating a new opportunities for",
    date: "18:00, 17/02/2023",
  },
];

export default function VideoSpotlightNew() {
  return (
    <div className="bg-DarkGray py-10">
      <Container>
        <div className="flex">
          <div className="mb-[1.875rem] relative shrink-0">
            <Image
              src="/images/news-page/Video Spotlight.png"
              alt="image"
              width={710}
              height={425}
              style={{ width: "100%", height: "auto", objectFit: "cover" }}
            />
            <div className="absolute left-[1.563rem] bottom-[1.563rem] z-10 flex flex-col gap-[0.938rem]">
              <div className="text-white leading-none font-semibold capitalize p-[0.313rem] bg-DarkRed max-w-fit">
                Consumer Electronic
              </div>
              <h2 className="text-[2.188rem] text-white leading-[120%] font-bold drop-shadow-[0px_4px_4px_rgba(0,0,0,0.25)]">
                Reed Tradex team visited Thai Garment Manufacturers Association
              </h2>
              <p className="text-GhostWhite leading-none font-medium italic">
                18:00, 17/02/2023
              </p>
            </div>
          </div>

          <div className="flex flex-col">
            {listNews.map((item) => {
              return (
                <div
                  key={item.id}
                  className="flex py-[0.938rem] border-b border-Gray border-solid"
                >
                  <div className="flex-1 flex flex-col pt-[0.938rem] pl-[1.563rem]  pr-[0.938rem] pb-[0.625rem] gap-[0.625rem]">
                    <div
                      className={`text-white text-[0.625rem] leading-none font-semibold capitalize p-[0.313rem] max-w-fit`}
                      style={{ backgroundColor: `${item.colorTag}` }}
                    >
                      {item.tag}
                    </div>
                    <h2 className="text-[0.938rem] text-black leading-[108.5%] font-bold">
                      {item.title}
                    </h2>
                    <p className="text-[#8B8B8B] text-[0.813rem] leading-none font-medium italic">
                      {item.date}
                    </p>
                  </div>

                  <div className="w-[8.438rem] h-[6.875rem] shrink-0">
                    <iframe
                      width="135"
                      height="110"
                      src="https://www.youtube.com/embed/3Imw4D358gI?controls=0"
                      title="YouTube video player"
                      frameborder="0"
                      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                      allowfullscreen
                    ></iframe>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </Container>
    </div>
  );
}
