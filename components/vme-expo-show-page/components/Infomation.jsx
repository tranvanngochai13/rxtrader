import Image from "next/image";
import React from "react";
import { Container } from "../../common/Container";

export default function Infomation() {
  return (
    <div className="bg-Gray py-10">
      <Container>
        <div className="flex flex-col gap-[1.875rem]">
          <h2 className="text-[2.5rem] text-white leading-[130%] text-center">
            Vietnam's Leading Exhibition on Machinery and Technology for
            Manufacturing and Supporting Industries
          </h2>
          <div className="flex items-center justify-between">
            <Image
              src="/images/VMEExpoShow/brands.png"
              alt="image"
              width={554.11}
              height={227.81}
              style={{ objectFit: "cover" }}
            />
            <Image
              src="/images/VMEExpoShow/countries.png"
              alt="image"
              width={554.11}
              height={227.81}
              style={{ objectFit: "cover" }}
            />
          </div>
          <div className="flex items-center justify-between">
            <Image
              src="/images/VMEExpoShow/Infor Graphics-1.png"
              alt="image"
              width={361.46}
              height={276.64}
              style={{ objectFit: "cover" }}
            />
            <Image
              src="/images/VMEExpoShow/Infor Graphics-2.png"
              alt="image"
              width={361.46}
              height={276.64}
              style={{ objectFit: "cover" }}
            />
            <Image
              src="/images/VMEExpoShow/Infor Graphics-3.png"
              alt="image"
              width={361.46}
              height={276.64}
              style={{ objectFit: "cover" }}
            />
          </div>
        </div>
      </Container>
    </div>
  );
}
