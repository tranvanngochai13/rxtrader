import Image from "next/image";
import React from "react";
import { Button } from "../../common/Button";
import { Container } from "../../common/Container";
import { LineTitle } from "../../common/vme-expo-show/LineTitle";

const listPost = [
  {
    id: 1,
    image: "/images/VMEExpoShow/Post-1.png",
  },
  {
    id: 2,
    image: "/images/VMEExpoShow/Post-2.png",
  },
  {
    id: 3,
    image: "/images/VMEExpoShow/Post-3.png",
  },
  {
    id: 4,
    image: "/images/VMEExpoShow/Post-4.png",
  },
];

export default function PreviousPostShowReport() {
  return (
    <div className="bg-bgGray py-10">
      <Container>
        <div className="flex flex-col gap-[1.875rem] text-center">
          <LineTitle title="Previous Post Show Report" colorLine="DarkRed" />
          <div className="grid grid-cols-4 gap-[1.417rem] justify-between">
            {listPost.map((item) => {
              return (
                <div
                  key={item.id}
                  className="flex flex-col gap-[1.25rem] items-center"
                >
                  <Image
                    key={item.id}
                    src={item.image}
                    alt="image"
                    width={267}
                    height={267}
                    style={{
                      width: "100%",
                      height: "auto",
                      objectFit: "cover",
                    }}
                  />
                  <Button
                    name="DOWNLOAD NOW"
                    background="#CD1719"
                    colorBorder="#9C1415"
                    uppercase
                  />
                </div>
              );
            })}
          </div>
        </div>
      </Container>
    </div>
  );
}
