import Image from "next/image";
import React from "react";
import { Container } from "../../common/Container";
import { LineTitle } from "../../common/vme-expo-show/LineTitle";

export default function IndustrialsNews() {
  return (
    <div className="bg-white py-10">
      <Container>
        <div className="flex flex-col gap-[1.875rem] text-center">
          <div className="flex flex-col gap-[1.25rem]">
            <LineTitle title="Industrial's News" colorLine="DarkRed" />
            <p className="text-[1.063rem] leading-[171%]">
              Optimize productivity and transform factory production lines with
              Vietnam Manufacturing Expo 2022, which brings together more than
              200 brands of machinery, equipment, and technology from more than
              20 countries with the presence of more than 10,000 manufacturers,
              manufacturing, and supporting industries to explore cutting-edge
              advancements under the Future Factory theme.
            </p>
          </div>

          <div className="flex flex-col gap-[1.875rem] text-center">
            <h3 className="text-[1.75rem] leading-[92.4%]">
              Latest eNewsletter
            </h3>
            <div className="flex items-center justify-between gap-[0.938rem]">
              <Image
                src="/images/VMEExpoShow/Latest eNewsletter-1.png"
                alt="image"
                width={370}
                height={205}
                style={{ objectFit: "cover" }}
              />
              <Image
                src="/images/VMEExpoShow/Latest eNewsletter-2.png"
                alt="image"
                width={370}
                height={205}
                style={{ objectFit: "cover" }}
              />
              <Image
                src="/images/VMEExpoShow/Latest eNewsletter-3.png"
                alt="image"
                width={370}
                height={205}
                style={{ objectFit: "cover" }}
              />
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
}
