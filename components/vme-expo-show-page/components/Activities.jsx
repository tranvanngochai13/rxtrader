import React from "react";
import { Button } from "../../common/Button";
import { Container } from "../../common/Container";
import { LineTitle } from "../../common/vme-expo-show/LineTitle";

const listButton = [
  {
    id: 1,
    title: "Innovation Showcase",
    desc: "Book your slot and showcase your product in this one-of-a-kind activity designed to demonstrate cutting-edge technology through on-site performance, with a timeslot spotlight on an LED screen. There are only a few performance slots available, for a good offer, reserve now!",
  },
  {
    id: 2,
    title: "Buyer Zone",
    desc: "Drop by and experience a mini-at-hall marketplace for business-to-business networking, where all Buyer and Seller demands will collide and lead to the formation of a future corporation. Don’t miss out and register today!",
  },
];

export default function Activities() {
  return (
    <div className="bg-bgGray py-10">
      <Container>
        <div className="flex flex-col gap-[1.875rem] text-center">
          <LineTitle title="Activities" colorLine="DarkRed" />
          <div className="grid grid-cols-2 gap-4 justify-between">
            {listButton.map((item) => {
              return (
                <div key={item.id} className="flex flex-col items-center gap-6">
                  <h3 className="text-[2.188rem] font-bold leading-[120%]">
                    {item.title}
                  </h3>
                  <p className="text-[1.063rem] leading-[120%]">{item.desc}</p>
                  <Button
                    name="DOWNLOAD NOW"
                    background="#CD1719"
                    colorBorder="#9C1415"
                    uppercase
                  />
                </div>
              );
            })}
          </div>
        </div>
      </Container>
    </div>
  );
}
