import React from "react";
import { Container } from "../../common/Container";
import { LineTitle } from "../../common/vme-expo-show/LineTitle";

export default function Featured() {
  return (
    <div className="bg-bgGray py-10">
      <Container>
        <div className="flex flex-col gap-[1.875rem] text-center">
          <LineTitle title="FEATURED EXHIBITORS 2022" colorLine="DarkRed" />
          <div className="grid grid-cols-3 gap-4 justify-between">
            {[0, 0, 0, 0, 0, 0].map((item, index) => {
              return (
                <div key={index} className="w-auto h-auto shrink-0">
                  <iframe
                    width="368"
                    height="209.67"
                    src="https://www.youtube.com/embed/3Imw4D358gI?controls=0"
                    title="YouTube video player"
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    allowfullscreen
                  ></iframe>
                </div>
              );
            })}
          </div>
        </div>
      </Container>
    </div>
  );
}
