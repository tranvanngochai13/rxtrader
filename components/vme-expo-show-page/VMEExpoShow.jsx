import React from "react";
import AboutRXTradex from "../about-rxtradex/AboutRXTradex";
import AdsSection from "../ads-section/AdsSection";
import ContactUs from "../contact-us/ContactUs";
import FollowUs from "../follow-us/FollowUs";
import ImportantNote from "../important-note/ImportantNote";
import ShowLocation from "../show-location/ShowLocation";
import Activities from "./components/Activities";
import Banner from "./components/Banner";
import Featured from "./components/Featured";
import IndustrialsNews from "./components/IndustrialsNews";
import Infomation from "./components/Infomation";
import PreviousPostShowReport from "./components/PreviousPostShowReport";

export default function VMEExpoShow() {
  const content =
    "RX Tradex is ASEAN's leading exhibition organizer. We are a member of RX, the world’s leading events organizer which creates high-profile, targeted industry events where buyers and suppliers come together to do business.";
  const backGround = "/images/VMEExpoShow/Contact Information.png";
  const importantNote = `“Vietnam Manufacturing Expo" is open to trade visitors only. Please dress in business attire. Those wearing shorts and/or sandals and minors under the age of 15 will not be permitted into the exhibition hall. The organizer reserves the right to refuse admission to anyone without cause or explanation.`;
  return (
    <>
      <Banner />
      <AdsSection backgroundColor="#F3F3F3" />
      <Infomation />
      <IndustrialsNews />
      <Activities />
      <Featured />
      <AboutRXTradex
        content={content}
        headingColor="#FFFFFF"
        contentColor="#FFFFFF"
        backGround="#1A1A1A"
        fontSize="1.063rem"
        lineHeight="120%"
      />
      <PreviousPostShowReport />
      <ContactUs backGround={backGround} />
      <ShowLocation
        showContent
        map="/images/VMEExpoShow/map.png"
        widthImage={1136}
        heightImage={421.54}
      />
      <FollowUs isShowBottom={true} />
      <ImportantNote backGround="#F0EFEF" importantNote={importantNote} />
    </>
  );
}
