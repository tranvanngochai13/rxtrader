import Image from "next/image";
import React from "react";
import { Container } from "../common/Container";
import { LineTitle } from "../common/vme-expo-show/LineTitle";

const listFollow = [
  {
    id: 1,
    name: "Contac Us",
    image: "/images/VMEExpoShow/call.png",
  },
  {
    id: 2,
    name: "/vietnammanufacturingexpopage",
    image: "/images/VMEExpoShow/facebook.png",
  },
  {
    id: 3,
    name: "Youtube Chanel",
    image: "/images/VMEExpoShow/youtube.png",
  },
  {
    id: 4,
    name: "VME Expo",
    image: "/images/VMEExpoShow/zalo.png",
  },
];

const listFollowBottom = [
  {
    id: 1,
    name: "Venue",
    image: "/images/VMEExpoShow/Venue.png",
  },
  {
    id: 2,
    name: "About Vietnam",
    image: "/images/VMEExpoShow/About Vietnam.png",
  },
  {
    id: 3,
    name: "VISA information",
    image: "/images/VMEExpoShow/VISA information.png",
  },
  {
    id: 4,
    name: "Recommended Hotel",
    image: "/images/VMEExpoShow/Recommended Hotel.png",
  },
];

export default function FollowUs({ isShowBottom = false }) {
  return (
    <>
      <div
        style={{
          backgroundImage: `url("images/VMEExpoShow/Follow Us.png")`,
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
        }}
        className="py-10"
      >
        <Container>
          <div className="flex flex-col gap-[1.875rem] relative">
            <LineTitle
              title="Follow Us"
              colorLine="DarkRed"
              textColor="white"
            />
            <div className="grid grid-cols-4 items-center justify-between gap-4">
              {listFollow.map((item) => {
                return (
                  <div
                    key={item.id}
                    className="flex flex-col items-center gap-[1.25rem]"
                  >
                    <Image
                      src={item.image}
                      alt="image"
                      width={80}
                      height={80}
                      style={{ objectFit: "cover" }}
                    />
                    <p className="text-white text-[1.063rem] font-bold leading-none">
                      {item.name}
                    </p>
                  </div>
                );
              })}
            </div>
          </div>
        </Container>
      </div>

      {isShowBottom && (
        <div className="bg-white py-10">
          <Container>
            <div className="grid grid-cols-4 items-center justify-between gap-4">
              {listFollowBottom.map((item) => {
                return (
                  <div
                    key={item.id}
                    className="flex flex-col items-center gap-[1.25rem]"
                  >
                    <Image
                      src={item.image}
                      alt="image"
                      width={80}
                      height={80}
                      style={{ objectFit: "cover" }}
                    />
                    <p className="text-[1.063rem] font-bold leading-none">
                      {item.name}
                    </p>
                  </div>
                );
              })}
            </div>
          </Container>
        </div>
      )}
    </>
  );
}
