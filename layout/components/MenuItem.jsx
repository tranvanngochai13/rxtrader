import React from "react";

export default function MenuItem({ item }) {
  return (
    <>
      <li className="hover-item group relative  px-10 cursor-pointer h-full flex items-center hover:bg-RedMenu">
        {item.name}
        <ul className="mega__menu hover-item absolute z-10 top-full left-0 bg-DarkBlack w-max capitalize opacity-0 invisible group-hover:opacity-100 group-hover:visible">
          {item?.subMenu?.map((sub) => {
            return (
              <li
                key={sub.id}
                className="px-4 py-[0.625rem] hover-item hover:bg-RedMenu"
              >
                {sub.name}
              </li>
            );
          })}
        </ul>
      </li>
    </>
  );
}
