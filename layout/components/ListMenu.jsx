import React from "react";
import MenuItem from "./MenuItem";

const listMenu = [
  {
    id: 1,
    name: "Về chúng tôi",
  },
  {
    id: 2,
    name: "Lịch Triển Lãm",
    subMenu: [
      {
        id: 1,
        name: "VME - VIETNAM MANUFACTURING EXPO 2023",
      },
      {
        id: 2,
        name: "NEV - NEPCON VIETNAM EXHIBITION 2023 IN HA NOI",
      },
      {
        id: 3,
        name: "NEV - Nepcon Vietnam Exhibition 2023 in Ho Chi Minh city",
      },
      {
        id: 4,
        name: "MXV - METALEX VIETNAM 2023​​",
      },
      {
        id: 5,
        name: "WRV - WASTE & RECYCLING EXPO VIETNAM 2023​​​",
      },
    ],
  },
  {
    id: 3,
    name: "Lịch Hội Nghị",
  },
  {
    id: 4,
    name: "Cơ Hội Giao Thương Quốc Tế",
  },
  {
    id: 5,
    name: "Tin Tức",
    subMenu: [
      {
        id: 1,
        name: "Tin Tổng Hợp",
      },
      {
        id: 2,
        name: "Tin Nổi Bật",
      },
    ],
  },
];

export default function ListMenu() {
  return (
    <ul className="hidden xl:flex items-center capitalize text-white text-[0.813rem] leading-[150%] h-[5.313rem]">
      {listMenu.map((item) => {
        return <MenuItem key={item.id} item={item} />;
      })}
    </ul>
  );
}
