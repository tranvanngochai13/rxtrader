import React from "react";
import NavBar from "./NavBar";
import TopHeader from "./TopHeader";

export default function Header() {
  return (
    <header>
      <TopHeader />
      <NavBar />
    </header>
  );
}
