import React, { useState } from "react";

const listMenu = [
  {
    id: 1,
    name: "Về chúng tôi",
  },
  {
    id: 2,
    name: "Lịch Triển Lãm",
    subMenu: [
      {
        id: 1,
        name: "VME - VIETNAM MANUFACTURING EXPO 2023",
      },
      {
        id: 2,
        name: "NEV - NEPCON VIETNAM EXHIBITION 2023 IN HA NOI",
      },
      {
        id: 3,
        name: "NEV - Nepcon Vietnam Exhibition 2023 in Ho Chi Minh city",
      },
      {
        id: 4,
        name: "MXV - METALEX VIETNAM 2023​​",
      },
      {
        id: 5,
        name: "WRV - WASTE & RECYCLING EXPO VIETNAM 2023​​​",
      },
    ],
  },
  {
    id: 3,
    name: "Lịch Hội Nghị",
  },
  {
    id: 4,
    name: "Cơ Hội Giao Thương Quốc Tế",
  },
  {
    id: 5,
    name: "Tin Tức",
    subMenu: [
      {
        id: 1,
        name: "Tin Tổng Hợp",
      },
      {
        id: 2,
        name: "Tin Nổi Bật",
      },
    ],
  },
];

export default function ListMenuMobile({ showMenu }) {
  const [heading, setHeading] = useState("");

  return (
    <ul
      className={`xl:hidden absolute ${
        showMenu ? "top-[76px] opacity-100" : "top-[-100vh] opacity-0"
      } duration-500 bg-DarkBlack  flex flex-col w-full gap-4 capitalize text-white text-base leading-[150%] p-4`}
    >
      {listMenu.map((item) => {
        return (
          <div key={item.id} className="flex flex-col">
            <li
              className="flex justify-between items-center hover-item cursor-pointer hover:bg-RedMenu py-[0.625rem] px-2"
              onClick={() =>
                heading !== item.name ? setHeading(item.name) : setHeading("")
              }
            >
              {item.name}
              <span
                className={`${
                  heading === item.name ? "rotate-180" : ""
                } w-[18px] h-[18px] overflow-hidden transition-all duration-150 ease-linear`}
              >
                {item.subMenu && (
                  <ion-icon name="chevron-up-outline"></ion-icon>
                )}
              </span>
            </li>
            <ul
              className={`capitalize ${
                heading === item.name
                  ? " translate-x-0 opacity-100"
                  : " -translate-x-10 opacity-0"
              } transition-all duration-300 ease-linear`}
            >
              {heading === item.name && (
                <div>
                  {item?.subMenu?.map((sub) => {
                    return (
                      <li
                        key={sub.id}
                        className="px-4 py-[0.625rem] hover-item hover:bg-RedMenu"
                      >
                        {sub.name}
                      </li>
                    );
                  })}
                </div>
              )}
            </ul>
          </div>
        );
      })}
    </ul>
  );
}
