import Image from "next/image";

export function BackToTop() {
  return (
    <div>
      <Image
        src="/images/Back-to-top/back-to-top.png"
        alt=""
        width={50}
        height={50}
        style={{ objectFit: "cover" }}
      />
    </div>
  );
}
