import Image from "next/image";
import React from "react";
import { Container } from "../../components/common/Container";

const listFooterLeft = [
  {
    id: 1,
    name: "METALEX",
  },
  {
    id: 2,
    name: "Manufacturing Expo",
  },
  {
    id: 3,
    name: "InterMold Thailand",
  },
  {
    id: 4,
    name: "InterPlas Thailand",
  },
  {
    id: 5,
    name: "Automotive Manufacturing",
  },
  {
    id: 6,
    name: "Assembly Technology",
  },
  {
    id: 7,
    name: "NEPCON Thailand",
  },
  {
    id: 8,
    name: "Surface & Coatings",
  },
  {
    id: 9,
    name: "REX ASEAN Robomation Expo",
  },
  {
    id: 10,
    name: "FACTECH",
  },
  {
    id: 11,
    name: "ROBOT X",
  },
];

const listFooterRight = [
  {
    id: 1,
    group: [
      {
        id: 1,
        name: "GFT EXPO",
      },
    ],
  },
  {
    id: 2,
    group: [
      {
        id: 1,
        name: "TILOG - LOGISTIX",
      },
    ],
  },
  {
    id: 3,
    group: [
      {
        id: 1,
        name: "ThaiCorrugated",
      },
      {
        id: 2,
        name: "ThaiFoldingCarton",
      },
    ],
  },
  {
    id: 4,
    group: [
      {
        id: 1,
        name: "COSMEX",
      },
    ],
  },
  {
    id: 5,
    group: [
      {
        id: 1,
        name: "VietCorrugated",
      },
      {
        id: 2,
        name: "TVietFoldingCarton",
      },
    ],
  },
  {
    id: 6,
    group: [
      {
        id: 1,
        name: "Vietnam Manufacturing Expo",
      },
    ],
  },
  {
    id: 7,
    group: [
      {
        id: 1,
        name: "Factory Logistix Vietnam",
      },
    ],
  },
  {
    id: 8,
    group: [
      {
        id: 1,
        name: "NEPCON Vietnam",
      },
    ],
  },
  {
    id: 9,
    group: [
      {
        id: 1,
        name: "METALEX Vietnam",
      },
    ],
  },
  {
    id: 10,
    group: [
      {
        id: 1,
        name: "Welding Vietnam",
      },
    ],
  },
];

const listrRules = [
  {
    id: 1,
    name: "Chính sách Cookie",
  },
  {
    id: 2,
    name: "Điều khoản bảo mật",
  },
  {
    id: 3,
    name: "Điều khoản về bản quyền",
  },
  {
    id: 4,
    name: "Terms & Conditions",
  },
  {
    id: 5,
    name: "Cookie Settings",
  },
];

const listInfo = [
  {
    id: 1,
    name: "Về chúng tôi",
  },
  {
    id: 2,
    name: "Lịch Triển Lãm",
  },
  {
    id: 3,
    name: "Lịch Hội Nghị",
  },
  {
    id: 4,
    name: "Điểm Tin Trong Ngày",
  },
  {
    id: 5,
    name: "Cơ Hội Giao Thương Quốc Tế",
  },
  {
    id: 6,
    name: "Tin Tức",
  },
  {
    id: 7,
    name: "Sơ đồ trang",
  },
];

export default function FooterTop() {
  return (
    <div className="bg-DarkBlack w-full pt-10 pb-5">
      <Container>
        <div className="flex flex-col lg:flex-row gap-4 pb-8 border-b-2 border-[#3F3F3F] border-solid">
          <div className="w-full lg:w-[23rem] flex flex-col gap-[0.688rem]">
            <Image
              src="/images/Footer/logo.png"
              alt=""
              width={265}
              height={44}
              style={{ objectFit: "cover", cursor: "pointer" }}
            />
            <p className="text-LightSlateGray">
              RX Tradex là đơn vị tổ chức triển lãm hàng đầu châu Á. Chúng tôi
              tự hào là thành viên của RX, công ty tổ chức sự kiện hàng đầu thế
              giới chuyên mang đến những sự kiện cao cấp, chuyên sâu, kết nối
              người mua đến với các nhà cung cấp, đối tác kinh doanh chiến lược.
              Với quy mô toàn cầu hơn 400 sự kiện tại 22 quốc gia, phục vụ 43
              lĩnh vực công nghiệp. Riêng tại Thái Lan và Việt Nam, chúng tôi tổ
              chức 20 triển lãm và hội nghị quốc tế uy tín.
            </p>
            <h3 className="text-GhostWhite text-[1.094rem] font-semibold leading-[120%]">
              38 năm thành công
            </h3>
            <p className="text-LightSlateGray text-[0.938rem] leading-[147%] tracking-[.01em]">
              Với 38 năm, RX Tradex đã và đang thiết lập hệ thống kết nối mạng
              lưới các doanh nghiệp đa dạng trong nhiều ngành công nghiệp, tối
              ưu bằng phương thức kết hợp các yếu tố Chiến lược sự kiện, Kiểm
              soát tài chính, Vận hành tinh gọn và Truyền thông tích hợp, chứng
              minh hiệu quả nhờ vào kinh nghiệm và sự kết nối.
            </p>
          </div>
          <div className="w-full lg:w-[29rem]">
            <h3 className="text-LightSlateGray text-[1.094rem] leading-[120%] tracking-[-0.01em] capitalize font-bold mb-[0.625rem]">
              Liên Kết Với Các Triển Lãm
            </h3>
            <div className="flex flex-col lg:flex-row gap-10">
              <div className="text-GhostWhite capitalize font-semibold text-sm leading-[157%]">
                <ul className="flex flex-col gap-3">
                  {listFooterLeft.map((item) => {
                    return <li key={item.id}>{item.name}</li>;
                  })}
                </ul>
              </div>
              <div className="text-GhostWhite capitalize font-semibold text-sm leading-[150%]">
                <ul className="flex flex-col gap-[0.938rem]">
                  {listFooterRight.map((item) => {
                    return (
                      <li key={item.id}>
                        {item.group.map((group, index) => {
                          return (
                            <div key={group.id}>
                              {item.group.length > 1 && index === 0 ? (
                                <>
                                  <span>{group.name}</span>
                                  <span> & </span>
                                </>
                              ) : (
                                <span>{group.name}</span>
                              )}
                            </div>
                          );
                        })}
                      </li>
                    );
                  })}
                </ul>
              </div>
            </div>
          </div>
          <div className="w-full lg:w-[17rem]">
            <h3 className="text-LightSlateGray text-[1.094rem] leading-[120%] tracking-[-0.01em] capitalize font-bold mb-[0.625rem]">
              Liên hệ với chúng tôi
            </h3>
            <div className="mb-[0.625rem]">
              <h4 className="text-[0.938rem] text-white leading-[150%] font-bold mb-[0.625rem]">
                Văn phòng Thái Lan
              </h4>
              <div className="text-LightSlateGray leading-[150%]">
                <p>
                  Tầng 32, Tòa nhà Sathorn Nakorn, 100/68-69 North Sathon đường
                  Silom, Bangrak, Băng Cốc 10500 Thái Lan.
                </p>
                <div className="flex flex-wrap">
                  <span className="shrink-0 mr-1">Điện thoại:</span>
                  <span className="text-GhostWhite">+66 2686-7299</span>
                </div>
                <div className="flex flex-wrap">
                  <span className="shrink-0 mr-1">Hộp thư:</span>
                  <span className="text-GhostWhite">rtdxvn@reedtradex.vn</span>
                </div>
                <div className="flex flex-wrap">
                  <span className="shrink-0 mr-1">Trang web:</span>
                  <span className="text-GhostWhite">www.reedtradex.com</span>
                </div>
              </div>
            </div>
            <div className="mb-[0.938rem]">
              <h4 className="text-[0.938rem] text-white leading-[150%] font-bold mb-[0.625rem]">
                Văn phòng Việt Nam
              </h4>
              <div className="text-LightSlateGray leading-[150%]">
                <p>
                  Tầng 2, Kova Center, 92G-92H Nguyễn Hữu Cảnh, Phường 22, Quận
                  Bình Thạnh, TP. Hồ Chí Minh
                </p>
                <div className="flex flex-wrap">
                  <span className="shrink-0 mr-1">Điện thoại:</span>
                  <span className="text-GhostWhite">+84 28-6287-3355</span>
                </div>
                <div className="flex flex-wrap">
                  <span className="shrink-0 mr-1">Hộp thư:</span>
                  <span className="text-GhostWhite">
                    CustomerService@rxtradex.com
                  </span>
                </div>
              </div>
            </div>
            <div className="flex flex-wrap gap-x-[1.125rem] gap-y-[0.625rem] text-[0.813rem] text-GhostWhite">
              {listrRules.map((item) => {
                return (
                  <p key={item.id} className="min-w-[7.938rem]">
                    {item.name}
                  </p>
                );
              })}
            </div>
          </div>
        </div>

        <div className="text-GhostWhite flex flex-col lg:flex-row gap-[1.25rem] p-y-[0.313rem] mt-[0.813rem] mb-8">
          {listInfo.map((item) => {
            return <p key={item.id}>{item.name}</p>;
          })}
        </div>

        <div className="max-w-[26.75rem] text-GhostWhite text-[0.688] leading-[191%]">
          <p>
            Công ty thành viên của RX, đơn vị tổ chức triển lãm hàng đầu thế
            giới. © 2023Thông tin được sở hữu bởi Công ty TNHH Reed Tradex. Bản
            quyền được bảo lưu.
          </p>
        </div>
      </Container>
    </div>
  );
}
