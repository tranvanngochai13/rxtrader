import React from "react";
import FooterBottom from "./FooterBottom";
import FooterTop from "./FooterTop";

export default function Footer() {
  return (
    <footer>
      <FooterTop />
      <FooterBottom />
    </footer>
  );
}
