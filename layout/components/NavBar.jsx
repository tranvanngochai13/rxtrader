import Image from "next/image";
import React, { useState } from "react";
import { Container } from "../../components/common/Container";
import ListMenu from "./ListMenu";
import ListMenuMobile from "./mobile/ListMenuMobile";

export default function NavBar() {
  const [showMenu, setShowMenu] = useState(false);

  return (
    <div className="relative bg-DarkBlack flex items-center">
      <Container>
        <div className="flex flex-col xl:flex-row items-center justify-between gap-5 py-4 xl:py-[0.313rem]">
          <div className="w-full xl:w-auto flex items-center justify-between">
            <div className="relative w-[265px] h-[44px]">
              <Image
                src="/images/Header/logo.png"
                alt=""
                fill
                style={{ objectFit: "cover", cursor: "pointer" }}
              />
            </div>
            <div
              className="text-3xl w-[30px] h-[30px] leading-none xl:hidden"
              onClick={() => setShowMenu(!showMenu)}
            >
              <ion-icon name={showMenu ? "close" : "menu-outline"}></ion-icon>
            </div>
          </div>
          <ListMenu />
          <ListMenuMobile showMenu={showMenu} />
        </div>
      </Container>
    </div>
  );
}
