import Image from "next/image";
import React from "react";
import { Container } from "../../components/common/Container";

export default function FooterBottom() {
  return (
    <div className=" bg-[#202020] pt-[1.056rem] pb-[0.999rem]">
      <Container>
        <div className="w-full flex flex-col lg:flex-row gap-4 lg:gap-0 justify-between items-center">
          <Image
            src="/images/Footer/bottom-left.png"
            alt=""
            width={335.17}
            height={30}
            style={{ objectFit: "cover" }}
          />
          <Image
            src="/images/Footer/bottom-right.png"
            alt=""
            width={83.99}
            height={30}
            style={{ objectFit: "cover" }}
          />
        </div>
      </Container>
    </div>
  );
}
