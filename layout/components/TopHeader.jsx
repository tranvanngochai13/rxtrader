import Image from "next/image";
import React from "react";
import {
  FacebookIcon,
  LikedinIcon,
  TwitterIcon,
  YoutubeIcon,
} from "../../assets/svg";
import { Container } from "../../components/common/Container";

const listIcon = [
  {
    icon: <FacebookIcon />,
  },
  {
    icon: <TwitterIcon />,
  },
  {
    icon: <LikedinIcon />,
  },
  {
    icon: <YoutubeIcon />,
  },
];

export default function TopHeader() {
  return (
    <div className="bg-white">
      <Container>
        <div className="flex flex-col lg:flex-row gap-2 lg:gap-0 items-center justify-between py-[0.438rem]">
          <div className="flex items-center gap-[0.438rem] uppercase text-[0.563rem] leading-[167%]">
            <p>Trang chủ</p>
            <span className="h-3 w-[0.063rem] bg-DarkBlack"></span>
            <p>Liên hệ</p>
            <span className="h-3 w-[0.063rem] bg-DarkBlack"></span>
            <div className="flex items-center gap-[0.438rem]">
              <span>Ngôn ngữ:</span>

              <Image
                src="/images/Header/english.png"
                alt=""
                width={17}
                height={17}
                style={{ objectFit: "cover" }}
              />
              <Image
                src="/images/Header/vn.png"
                alt=""
                width={17}
                height={17}
                style={{ objectFit: "cover" }}
              />
            </div>
          </div>
          <div className="flex items-center gap-[0.625rem]">
            {listIcon.map((item, index) => {
              return <div key={index}>{item.icon}</div>;
            })}
          </div>
        </div>
      </Container>
    </div>
  );
}
