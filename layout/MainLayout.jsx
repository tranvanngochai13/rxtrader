import Footer from "./components/Footer";
import Header from "./components/Header";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ScrollToTop from "react-scroll-to-top";
import { BackToTop } from "./components/BackToTop";

export function MainLayout({ children }) {
  return (
    <div>
      <Header />
      <main>{children}</main>
      <ScrollToTop smooth component={<BackToTop />} />
      <Footer />
      <ToastContainer />
    </div>
  );
}
