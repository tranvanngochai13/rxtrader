import React from "react";
import AdsSection from "../../components/ads-section/AdsSection";
import { Container } from "../../components/common/Container";
import News from "../../components/news-page/News";
import VideoSpotlightNew from "../../components/news-page/VideoSpotlightNew";
import Sidebar from "../../components/sidebar/Sidebar";

export default function NewsPage() {
  return (
    <div>
      <Container>
        <h2 className="relative mt-10 mb-[1.625rem] text-[2.5rem] text-black leading-[130%] after:absolute after:content-[''] after:bottom-[-1.25rem] after:left-0 after:bg-DarkRed after:w-[10.25rem] after:h-[0.375rem]">
          Tin tức
        </h2>

        <div className="flex py-10 gap-[3.125rem]">
          <News />
          <Sidebar />
        </div>
      </Container>
      <VideoSpotlightNew />
      <AdsSection />
    </div>
  );
}
