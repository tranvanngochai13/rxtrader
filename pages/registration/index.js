import React from "react";
import Banner from "../../components/registration-page/components/Banner";
import Registration from "../../components/registration-page/Registration";
import ExhibitionSection from "../../components/section/exhibition-section/ExhibitionSection";

export default function index() {
  return (
    <div className="bg-bgGray">
      <Banner />
      <Registration />
      <ExhibitionSection registrationForm />
    </div>
  );
}
