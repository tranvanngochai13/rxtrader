import React from "react";
import VMEExpoShow from "../../components/vme-expo-show-page/VMEExpoShow";

export default function index() {
  return (
    <div>
      <VMEExpoShow />
    </div>
  );
}
