import BannerSection from "../components/section/banner-section/BannerSection";
import InfoSection from "../components/section/info-section/InfoSection";
import ExhibitionSection from "../components/section/exhibition-section/ExhibitionSection";

export default function Home() {
  return (
    <div>
      <BannerSection />
      <InfoSection />
      <ExhibitionSection />
    </div>
  );
}
