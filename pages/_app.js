import "../styles/globals.css";
import { Provider } from "react-redux";
import { store } from "../store";
import { MainLayout } from "../layout";
import { AnimatePresence } from "framer-motion";
import {
  Hydrate,
  QueryClient,
  QueryClientProvider,
} from "@tanstack/react-query";

// Create a client
const queryClient = new QueryClient();

export default function App(props) {
  const { Component, pageProps, session, settings } = props;
  const Layout = Component.Layout ?? MainLayout;

  const handleExitComplete = () => {
    if (typeof window !== "undefined") {
      window.scrollTo({ top: 0 });
    }
  };

  return (
    <QueryClientProvider client={queryClient}>
      <Hydrate state={pageProps.dehydratedState}>
        <Provider store={store}>
          <AnimatePresence exitBeforeEnter onExitComplete={handleExitComplete}>
            <Layout>
              <Component {...pageProps} />
            </Layout>
          </AnimatePresence>
        </Provider>
      </Hydrate>
    </QueryClientProvider>
  );
}
