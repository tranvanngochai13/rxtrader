import React from "react";
import METALEXEvent from "../../components/metalex-event-page/METALEXEvent";

export default function index() {
  return (
    <div className="bg-bgPage">
      <METALEXEvent />
    </div>
  );
}
