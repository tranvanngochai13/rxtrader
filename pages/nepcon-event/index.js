import React from "react";
import NEPCONEvent from "../../components/nepcon-event-page/NEPCONEvent";

export default function index() {
  return (
    <div className="bg-bgPage">
      <NEPCONEvent />
    </div>
  );
}
